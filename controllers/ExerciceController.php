<?php

class ExerciceController extends Controller {

    // Parameters from request
    public $params;
    public $categorieModel;
    public $exerciceId;
    public $AuthMiddleware;

    // Fields always required in forms
    public $required = ["title", "consigne"];

    function  __construct($params) {
        $this->params = $params;

        if (isset($this->params->id))
        {
            $this->exerciceId = $this->params->id;
        }

        $this->categorieModel = new CategorieModel();

        $this->AuthMiddleware = new AuthMiddleware();
    }

    /**
     * Display an exercice
     * @param Int $id Id of the exercice.
     */
    public function show() {
        $this->exerciceId = $this->params->id;

        // Check is there is an ID in URL
        if (empty($this->exerciceId)) { $this->error("Vous devez spécifier un exercice."); }

        $exercice = $this->getExercice()->fetch();
        
        // Check if there is any result
        if (!$exercice) { $this->error('Aucun exercice ne correspond à cette recherche.'); }

        // Get prev and next exercise
        $prev = $this->categorieModel->getPrevExercice($this->exerciceId)->fetch();
        $next = $this->categorieModel->getNextExercice($this->exerciceId)->fetch();

        // Get scripts content
        $sc = new ScriptController($this->exerciceId);
        $sc->loadClientScripts();
        $files = $sc->fileContent;

        // Get exercice fields
        $fields = $this->getExerciceFields();

        // Put files content inside $exercice
        foreach($files as $key => $file)
        {
            $exercice[$key] = $file;
        }

        // Load server side script
        if ($_POST) {
            ob_start();
                $sc->loadServerScript(FALSE, FALSE);
            $scriptResult = ob_get_contents();
            ob_clean();
        }

        $this->render('exercice/show', compact("exercice", "fields", "scriptResult", "prev", "next"));
    }

    /**
     * Get exercice datas.
     * @param Int $id Id of the exercice.
     * @return PDOStatement Exercice datas.
     */
    public function getExercice() {
        return $this->categorieModel->getExercice($this->exerciceId);
    }

    /**
     * Get customs fields on an exercice
     * @return PDOStatelent Customs fields
     */
    public function getExerciceFields() {
        return $this->categorieModel->getExerciceFields($this->exerciceId);
    }

    /**
     * Create a new exercice
     */
    public function create() {
        // Ensure user is login
        $this->AuthMiddleware->authenticate();
        
        if (!empty($_POST)) {
            $escaped = $this->escapeSpecialChars($_POST);

            $error = [];
            if (empty($escaped['name'])) { $error['name'] = "Il manque le nom de l'exercice."; }
            if (empty($escaped['consigne'])) { $error['consigne'] = "Il manque la consigne de l'exercice."; }
            if (empty($escaped['main_categorie']) || $escaped['main_categorie'] === "-1") { $error['main_categorie'] = "Il manque la catégorie de l'exercice."; }
            if (empty($escaped['sub_categorie']) || $escaped['sub_categorie'] === "-1") { $error['sub_categorie'] = "Il manque la sous catégorie de l'exercice."; }

            if (empty($error)) {
                $insertID = $this->register($escaped);

                // Copy scripts layer inside new script directory
                if ($escaped['create_script'])
                {
                    $sc = new ScriptController();
                    $sc->recursiveCopy("scripts/_default", "scripts/{$insertID}/");
                }

                // /!\ Redirection don't work with mkdir function
                header("Location: " . ROOT . "?page=exercice&action=show&id=$insertID");
                exit;
            }
        }

        $catModel = new CategorieModel();
        $mainCategories = $catModel->getMainCategories();
        $subCategories = $catModel->getAllSubCategories();

        $this->render('exercice/create', compact("mainCategories", "subCategories"));
    }

    /**
     * Register a new exercice into database.
     * @param Array $form Datas from form
     * @return Int Last insert ID
     */
    public function register($form) {
        $cat = new CategorieModel();
        return $cat->setExercice($form);
    }

    /**
     * Edit an existing work
     */
    public function edit() {
        // Ensure user is login
        $this->AuthMiddleware->authenticate();
        // Get exercice data
        $exercice = $this->getExercice()->fetch();
        $cfields = $this->getExerciceFields();

        // Check if user send data from POST form
        if (!empty($_POST)) {

            // Customs fields
            if (isset($_POST['customField'])) {
                $this->manageCustomsFields($_POST['customField']);
                unset($_POST['customField']);
            }

            // Convert html tags
            $escaped = $this->escapeSpecialChars($_POST);

            // Check required fields
            $error = false;
            if(empty($escaped['name'])) { $error = true;}
            if(empty($escaped['consigne'])) { $error = true;}

            // Reset work status if needed
            if(empty($escaped['status'])) { $escaped['status'] = 0; }
            
            // Update bdd with new fields values
            if($error === false) { $update = $this->update($exercice['id'], $escaped); }

            // Redirect user
            if(isset($update)) { header('Location: ' . ROOT . "?page=exercice&action=show&id={$exercice['id']}"); }
        }

        $this->render('exercice/edit', compact("exercice", "cfields"));
    }

    /**
     * Update an existing work with new data
     * @param Array $fields New data from POST form
     * @return PDOStatement
     */
    public function update($id, $fields) {
        $model = new CategorieModel();
        return $model->updateExercice($id, $fields);
    }

    // TODO: Escape strings from fields

    /**
     * Manage customs fields: [INSERT | UPDATE | DELETE]
     * @param Array $formatedFields Fields data from form
     */
    public function manageCustomsFields($formFields) {
        $catModel = new CategorieModel();

        if ($formFields)
        {
            $formatedFields = [];
            foreach($formFields as $key => $formField) {

                // If there is already exist in BDD
                // Juste change field_name
                // But still with the same slug (input-hidden)
                if (!empty($formField['slug']))
                {
                    if (!empty($formField['name']))
                    {
                        // Rename field
                        $formatedFields['slug'] = $formField['slug'];
                        $formatedFields['exercice_id'] = $this->exerciceId;
                        $formatedFields['field_name'] = $formField['name'];
                        $formatedFields['placeholder'] = $formField['placeholder'];
                        $catModel->updateExerciceFields($this->exerciceId, $formatedFields);
                    }
                    else
                    {
                        // Delete field
                        $catModel->deleteExerciceField($formField['slug']);
                    }
                }
                else
                {
                    // Don't create an empty field
                    if (!empty($formField))
                    {
                        $formatedFields['slug'] = $formField['name'] . "-" . $this->exerciceId;
                        $formatedFields['exercice_id'] = $this->exerciceId;
                        $formatedFields['field_name'] = $formField['name'];
                        $formatedFields['placeholder'] = $formField['placeholder'];
                        $catModel->updateExerciceFields($this->exerciceId, $formatedFields);
                    }
                }
            }
        }
    }

    public function error($message) {
        $this->render('error/common', compact("message"));
        return;
    }
}
