<?php

class ScriptController {

    // Enumerate all needed files and their extensions
    public $manifest = [
        'javascript' => 'javascript.js',
        'jquery' => 'jquery.js',
        'php' => 'php.php'
    ];

    // Paths of each files
    public $filePath;

    // Content of each files
    public $fileContent;

    function __construct($exerciceId = NULL) {
        if (isset($exerciceId))
        {
            $this->exerciceId = $exerciceId;
            $this->loadFilePath();
            $this->getScriptsContent();
        }
    }

    /**
     * Get file content
     * @param String $filePath Path of the file
     * @return Mixed False | File content
     */
    public function getFileContent($filePath) {
        // Check if file exist
        if (!file_exists($filePath)) { return; }

        // Get file lines in array
        $lines = file($filePath);

        // Stock all lines in one variable
        $content = "";
        foreach ($lines as $line)
        {
            $content .= htmlentities($line);
        }

        return $content;
    }

    /**
     * Dynamicly declare files path from manifest
     */
    public function loadFilePath() {
        foreach ($this->manifest as $key => $filename) {
            $this->filePath[$key] = "scripts/" . $this->exerciceId . "/$filename";
        }
    }

    /**
     * Get scripts content for all are in the manifest
     */
    public function getScriptsContent() {
        foreach($this->filePath as $filename => $path)
        {
            $this->fileContent[$filename] = $this->getFileContent($path);
        }

        return $this->fileContent;
    }

    /**
     * Load client side scripts
     */
    public function loadClientScripts() {
        foreach($this->fileContent as $filename => $content) {
            if ($filename != "php" && !empty($content)) {
                echo '<script src="'. $this->filePath[$filename] . '"></script>';
            }
        }
    }

    /**
     * Load server side script
     * @param Bool $escapeFields Specify FALSE to not escape strings inside form. [Default = TRUE]
     * @param Bool $checkPresence Specify FALSE to not check values presence inside form fields. [Default = TRUE]
     */
    public function loadServerScript($escapeFields = TRUE, $checkPresence = TRUE) {
        $error = false;
        
        if ($escapeFields) {
            $this->checkForm();
        }

        if ($checkPresence) {
            $error = $this->checkDataPresence();
        }

        if (!$error) {
            require_once $this->filePath['php'];
        }
    }

    /**
     * Escape bad strings
     * /!\ Reptition with Controller.php
     * I should create a common trait file.
     */
    public function checkForm() {
        foreach($_POST as $key => $value)
        {
            $_POST[$key] = htmlentities($value);
        }
    }

    /**
     * Check data presence inside $_POST form
     * @return Bool True if data is missing.
     */
    public function checkDataPresence() {
        foreach($_POST as $key => $value)
        {
            if (empty($value)) { return TRUE; }
        }
        
        return FALSE;
    }

    /**
     * Copy recursivly a directory and all files inside.
     * https://www.php.net/manual/en/function.copy.php#91010
     * @param String $path Path to the new directory
     */
    public function recursiveCopy($src,$dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) )
        {
            // Escape parent and actual dirs
            if (( $file != '.' ) && ( $file != '..' )) { 
                if ( is_dir($src . '/' . $file) ) {
                    recurse_copy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
}