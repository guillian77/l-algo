<?php

class UserController extends Controller {

    use Configuration;

    public $error = [];
    public $fieldLength = 5;
    public $model;

    function __construct() {
        $this->model = new UserModel();
    }

    /**
     * Login a user
     */
    public function login() {
        if ($_POST) {
            // Check form
            $this->checkForm($_POST);

            if (!$this->error)
            {
                // Look for an existing user
                $user = $this->model->getUserByName($_POST['username'])->fetch();

                if(password_verify($_POST['password'], $user['password']))
                {
                    // Set any parameters inside session
                    $_SESSION['uid'] = $user['uid'];
                    $_SESSION['username'] = $user['username'];

                    // Redirection
                    header('Location: ' . ROOT);
                }
            }
        }

        // Render
        $errors = $this->error;
        $this->render('user/login', compact("errors"));
    }

    /**
     * Logout a user
     */
    public function logout() {
        // Destroy session content
        session_destroy();

        // Redirection
        header('Location: ' . ROOT);
    }

    /**
     * Register new user
     */
    public function register() {
        if($this->allow_registering  === FALSE) { $this->error("L'enregistrement n'est pas activé pour le moment."); }

        if($_POST)
        {
            // Look for errors
            $this->checkForm($_POST, TRUE);

            // There is no errors
            if (!$this->error) {

                // Hash password
                $_POST['password'] = password_hash($_POST['password'], PASSWORD_BCRYPT);

                // Try to save user to BDD
                $registered = $this->model->register($_POST);

                // Redirection or not
                if ($registered)
                {
                    header('Location: ' . ROOT);
                }
                else
                {
                    array_push($this->error, "Cannot be registered, an error has been occured.");
                }
            }
        }

        // Render
        $errors = $this->error;
        $this->render('user/register', compact("errors"));
    }

    /**
     * Check form
     * @param Array $form
     * @return Void
     */
    public function checkForm($form, $checkPassword = FALSE) {
        foreach ($form as $key => $field) {
            if (strlen($field) < $this->fieldLength) {
                array_push($this->error, "$key should be {$this->fieldLength} caracters length.");
            }
        }

        if (!$checkPassword) { return; }

        if ($form['password'] != $form['password_confirm']) {
            array_push($this->error, "Passwords are not the same.");
        }
    }
}