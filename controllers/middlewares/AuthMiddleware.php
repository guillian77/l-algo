<?php

class AuthMiddleware extends Controller {

    public function authenticate() {
        if (empty($_SESSION['uid'])) {
            $this->error("Vous devez être connecté pour voir cette page.");
            die();
        }
    }

}