<?php

class AdminController extends Controller {

    public $AuthMiddleware;

    public function __construct() {
        $this->AuthMiddleware = new AuthMiddleware();
        $this->AuthMiddleware->authenticate();
    }

    public function index() {
        $gitOutput = NULL;

        if ($_POST) {
            if (!empty($_POST['git_pull'])) {
                $gitOutput = $this->gitPull();
            }
        }
        

        $this->render('admin/index', compact("gitOutput"));
    }

    public function gitPull() {
        return exec('git pull');
    }
}