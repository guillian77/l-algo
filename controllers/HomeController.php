<?php

class HomeController extends Controller {

    public $done_limit = 10;
    public $inwork_limit = NULL;

    function  __construct() {
        $model = new CategorieModel();

        $dones = $model->getExercicesByStatus(1, $this->done_limit);
        $inworks = $model->getExercicesByStatus(0, $this->inwork_limit);

        $done_limit = $this->done_limit;
        $inwork_limit = $this->inwork_limit;
        $this->render('home/welcome', compact("dones", "inworks", "done_limit"));
    }
}
