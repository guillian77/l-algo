<?php

class Controller {

    /**
     * Render a view with layout
     * @param String $vue The path of view to render
     * @return void
     */
    public function render($vue, $datas = NULL) {

        if ($datas) {
            foreach ($datas as $key => $data) {
                // Create objects with sub arrays
                if (is_array($data))
                {
                    $var = $key;
                    $$var = (object) $data;
                }
                // Create variable named by array key
                else
                {
                    $var = $key;
                    $$var = $data;
                }
            }
        }

        ob_start();
            require "views/$vue.php";
        $content = ob_get_contents();
        ob_end_clean();

        // Bad practice :(
        $categorie = new CategorieModel();

        require "views/layout/default.php";
    }

    /**
     * Debug something and continue code execution
     * @param Mixed $somethingToDebug Variable to debug
     */
    public function debug($somethingToDebug, $die = NULL) {
        echo '<h1 style="background-color: grey; color:#fff; padding: 15px;">Debugging: Start</h1>';
        echo '<pre>';
        print_r($somethingToDebug);
        echo '</pre>';
        echo '<h1 style="background-color: grey; color:#fff; padding: 15px;">Debugging: End</h1>';
        if ($die) { die();}
    }
    
    /**
     * Debug something and stop code execution
     * @param Mixed $somethingToDebug Variable to debug
     */
    public function dd($somethingToDebug = NULL) {
        $this->debug($somethingToDebug, true);
    }

    /**
     * Convert html tags to lisible caracters and prevent bad intentions from user
     * @param Mixed $entity Array or string to escape.
     */
    public function escapeSpecialChars($entity) {
        if (is_array($entity)) {
            foreach($entity as $key => $string) {
                $entity[$key] = htmlentities($string, ENT_QUOTES);
            }
            return $entity;
        }
        return htmlentities($entity, ENT_QUOTES);
    }

    /**
     * Display an error page and die after.
     * @param String $message Message to display.
     */
    public function error($message) {
        $this->render('error/common', compact("message"));
        die();
    }
}