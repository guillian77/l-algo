<?php

class Model {

    use Configuration;

    public $db;

    public function __construct() {
        try {
            $this->db = new PDO('mysql:host='.$this->dbInfo['host'].';dbname='.$this->dbInfo['name'].'', $this->dbInfo['user'], $this->dbInfo['password']);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db->exec("SET NAMES 'utf8';");
            // var_dump('Appel à PDO');
        }
        catch (PDOException $e) {
            if ($this->mode === "dev") {
                echo '<pre>';
                echo 'Échec lors de la connexion : ' . $e->getMessage();
                echo '</pre>';
            }
            exit;
        }
    }

    /**
     * @param $table String Le nom de la table sur laquelle faire la requête.
     * @param $params String Paramètres supplémentaites à la requête.
     */
    public function get($table, $params = null) {
        $req = "SELECT * FROM $table";

        if ($params) { $req .= " " . $params; }

        $stmt = $this->db->query($req);

        return $stmt;
    }

    // IN WORK
    // public function insert(String $table, $req) {
    //
    // }

    /**
     * Update table with new data
     * @param String $table Table to update
     * @param Array $data Data to replace with
     * @param String $params Optionnals request parameters
     * @return PDOStatement
     */
    public function update($table, $data, $params = NULL) {
        // Get number of the last array key
        $lastKeyNum = count($data);
        $count = 0;

        // Create the request
        $req = "UPDATE $table SET ";
        foreach($data as $key => $field)
        {
            $count++;

            if ($count === $lastKeyNum) {
                $req .= " $key=:$key";
            } else {
                $req .= "$key=:$key ,";

            }
        }

        // Add optionnals parameters to request
        if ($params) { $req .= " $params"; }
        
        $stmt = $this->db->prepare($req);

        return $stmt->execute($data);
    }

    // IN WORK
    // public function delete() {

    // }
}