<?php

class UserModel extends Model {
    
    function __construct() {
        parent::__construct();
    }

    /**
     * Get user by name
     * @param String $username Username
     * @return PDOStatement
     */
    public function getUserByName($username) {
        $req = "SELECT * FROM algo_users WHERE username = '$username'";
        return $this->db->query($req);;
    }

    /**
     * Register new user
     * @param Array $data Data from form.
     */
    public function register($data) {
        $req = "INSERT INTO algo_users (username, password) VALUES (:username, :password)";
        $stmt = $this->db->prepare($req);
        $stmt->bindParam(':username', $data['username']);
        $stmt->bindParam(':password', $data['password']);
        // $statement->execute($data);
        $stmt->execute();
        return $this->db->lastInsertId();
    }
}