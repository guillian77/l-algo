<?php

class CategorieModel extends Model {

    // TODO: Return objects instead of array in PDOStatement. For better call inside of views.

    function __construct() {
        // Do not erase parent constructor with child
        parent::__construct();
    }

    /**
     * -------------------------------------------------------------------------
     * - Categories
     * -------------------------------------------------------------------------
     */

    /**
     * Get mains categories
     * @return PDOStatement Main categories
     */
    public function getMainCategories() {
        $params = "WHERE parent IS NULL";
        return $this->get('algo_categories', $params);
    }

    /**
     * Get sub categories by their parent categorie
     * @param Int $parentCategorie Id of parent categorie
     * @return PDOStatement Sub categories in one parent categorie
     */
    public function getSubCategories($parentCategorie) {
        $params = "WHERE parent = $parentCategorie";
        return $this->get('algo_categories', $params);
    }

    /**
     * Get all sub categories
     * @return PDOStatement Sub categories
     */
    public function getAllSubCategories() {
        $params = "WHERE parent IS NOT NULL";
        return $this->get('algo_categories', $params);
    }

    /**
     * -------------------------------------------------------------------------
     * - Exercices
     * -------------------------------------------------------------------------
     */

    /**
     * Get exercices inside of a categorie
     * @param Int $parentCategorie The ID of the categorie
     * @return PDOStatement Exercices
     */
    public function getExercicesInCategorie($parentCategorie) {
        $params = "WHERE categorie = $parentCategorie";
        return $this->get('algo_exercices', $params);
    }

    /**
     * Get an exercice with his ID
     * @param Int $id Exercice ID
     * @return PDOStatement Exercice datats
     */
    public function getExercice($exerciceId) {
        $params = "WHERE id = $exerciceId";
        return $this->get('algo_exercices', $params);
    }

    /**
     * Get previous exercice from a current one.
     * @param Int $currentId Current exercise ID.
     * @return PDOStatement
     */
    public function getPrevExercice($currentId) {
        $request = "SELECT id, name FROM algo_exercices WHERE id = (SELECT max(id) FROM algo_exercices WHERE id < $currentId)";
        return $this->db->query($request);
    }

    /**
     * Get next exercice from a current one.
     * @param Int $currentId Current exercise ID.
     * @return PDOStatement
     */
    public function getNextExercice($currentId) {
        $request = "SELECT id, name FROM algo_exercices WHERE id = (SELECT min(id) FROM algo_exercices WHERE id > $currentId)";
        return $this->db->query($request);
    }

    /**
     * Get all exercices by status
     * @param Int $status [1 : Done | 0 : Inwork]
     * @param Int $limit Maximum request results
     * @return PDOStatement
     */
    public function getExercicesByStatus(Int $status, Int $limit = NULL) {
        $params = "WHERE status = $status ORDER BY id DESC";

        if ($limit) { $params .= " LIMIT $limit"; }
        
        return $this->get('algo_exercices', $params);
    }

    /**
     * Register an exercice
     * @param Array $form Form from user
     * @return Int
     */
    public function setExercice($form) {
        $req = "INSERT INTO algo_exercices (name, categorie, consigne, pseudo_code) VALUES (:name, :categorie, :consigne, :pseudo_code)";
        $statement = $this->db->prepare($req);
        $statement->bindParam(':name', $form['name']);
        $statement->bindParam(':categorie', $form['sub_categorie']);
        $statement->bindParam(':consigne', $form['consigne']);
        $statement->bindParam(':pseudo_code', $form['pseudo_code']);
        $statement->execute();
        return $this->db->lastInsertId();
    }

    /**
     * Update a existings work with new data.
     * @param Int $id Id of work
     * @param Array $fields Data from POST form
     */
    public function updateExercice(Int $id ,Array $fields) {
        return $this->update('algo_exercices', $fields, "WHERE id = $id");
    }

    /**
     * -------------------------------------------------------------------------
     * - Exercices: Customs Fields
     * -------------------------------------------------------------------------
     */

    /**
     * Update or create a custom field
     * @param Int $exerciceId Exercise ID
     * @param Array $data Customs fields data
     */
    public function updateExerciceFields($exerciceId, $data) {
        $req = 'INSERT INTO algo_exercices_fields (slug, exercice_id, field_name, placeholder)';
        $req .= ' VALUE ("' . $data['slug'] . '", ' . $data['exercice_id'] . ', "' . $data['field_name'] . '", "' . $data['placeholder'] . '")';
        $req .= ' ON DUPLICATE KEY UPDATE field_name = "' . $data['field_name'] . '", placeholder = "' . $data['placeholder'] . '"';
        return $this->db->query($req);
    }

    /**
     * Get fields list for an exercice
     * @param Int $exerciceId Exercice ID
     * @return PDOStatement
     */
    public function getExerciceFields($exerciceId) {
        $req = "SELECT * FROM algo_exercices_fields";
        $req .= " WHERE exercice_id = $exerciceId";
        return $this->db->query($req);
    }

    /**
     * Delete a custom field from an exercise
     * @param String $slug Field slud
     * @return PDOStatement
     */
    public function deleteExerciceField($slug) {
        $req = "DELETE FROM algo_exercices_fields WHERE slug = '$slug'";
        return $this->db->query($req);
    }

}