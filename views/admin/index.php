<section class="main__content">
    <div class="content__header">
        <h2 class="main__title">Administration</h2>
    </div>

    <form action="" method="POST" class="mt-1">
        <label class="checkbox mt-5" for="git_pull">
            <input type="checkbox" name="git_pull" id="git_pull" />
            <span class="checkbox__round"></span>
            <span class="checkbox__label">Cocher mettre à jour l'application</span>
        </label>

        <p><?= (!empty($gitOutput)) ? "Résultat: " . $gitOutput: ""; ?></p>

        <input class="btn btn-primary" type="submit" value="Mettre à jour">
    </form>
</section>