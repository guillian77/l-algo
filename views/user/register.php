<!-- BREADCRUMB: Start -->
<nav class="breadcrumb">
    <a class="breadcrumb__item start" href="<?= ROOT; ?>"><i class="fas fa-home"></i></a>
    <span class="breadcrumb__item separator"><i class="fas fa-arrow-right"></i></span>
    <a class="breadcrumb__item">S'enregistrer</a>
</nav>
<!-- BREADCRUMB: End -->

<section class="main__content">
    <!-- CONTENT HEADER: Start -->
    <div class="content__header">
        <h2 class="main__title">S'enregistrer</h2>
    </div>
    <!-- CONTENT HEADER: Start -->

    <?php if(!empty($errors)): ?>
        <?php foreach($errors as $error): ?>
            <p><?= $error; ?></p>
        <?php endforeach; ?>
    <?php endif; ?>

    <form action="" method="POST" class="mt-1">
        <input class="input input-lg mt-1" type="text" name="username" placeholder="Nom d'utilisateur" id="username" />
        <input class="input input-lg mt-1" type="password" name="password" placeholder="Mot de passe" id="password" />
        <input class="input input-lg mt-1" type="password" name="password_confirm" placeholder="Mot de passe" id="password_confirm" />

        <input class="btn btn-primary mt-2" type="submit" value="Créer un compte" />
    </form>
</section>