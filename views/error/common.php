<!-- BREADCRUMB: Start -->
<nav class="breadcrumb">
    <a class="breadcrumb__item start" href="/"><i class="fas fa-home"></i></a>
    <span class="breadcrumb__item separator"><i class="fas fa-arrow-right"></i></span>
    <a class="breadcrumb__item">Exercices</a>
    <span class="breadcrumb__item separator"><i class="fas fa-arrow-right"></i></span>
    <a class="breadcrumb__item current">Introuvable</a>
</nav>
<!-- BREADCRUMB: End -->

<section class="main__content">
    <h2 class="main__title">Erreur</h2>
    <p><?= $message; ?></p>
</section>