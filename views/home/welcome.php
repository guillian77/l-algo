<!-- BREADCRUMB: Start -->
<nav class="breadcrumb">
    <a class="breadcrumb__item start" href="<?= ROOT; ?>"><i class="fas fa-home"></i></a>
</nav>
<!-- BREADCRUMB: End -->

<!-- Work: In progress -->
<section class="main__content">
    <div class="content__header">
        <h2 class="main__title">Travaux en cours</h2>
    </div>

    <div class="list-container">
        <?php foreach($inworks as $inwork): ?>
            <a class="list__line" href="<?= ROOT; ?>?page=exercice&action=show&id=<?= $inwork['id']; ?>">
                <i class="fas fa-code"></i>
                <h3 class="line__title"><?= $inwork['name']; ?></h3>
                <p class="line__description"><?= substr($inwork['consigne'], 0, 200); ?><?= (strlen($inwork['consigne']) > 200) ? " ..." : "" ; ?></p>
            </a>
        <?php endforeach; ?>
    </div>
</section>

<!-- Work: Done -->
<section class="main__content mt-5">
    <div class="content__header">
        <h2 class="main__title">Travaux terminés (<?= $done_limit; ?>)</h2>
    </div>

    <div class="list-container">
        <?php foreach($dones as $done): ?>
            <a class="list__line" href="<?= ROOT; ?>?page=exercice&action=show&id=<?= $done['id']; ?>">
                <i class="fas fa-code"></i>
                <h3 class="line__title"><?= $done['name']; ?></h3>
                <p class="line__description"><?= substr($done['consigne'], 0, 200); ?><?= (strlen($done['consigne']) > 200) ? " ..." : "" ; ?></p>
            </a>
        <?php endforeach; ?>
    </div>
</section>