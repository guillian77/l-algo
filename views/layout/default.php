<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>L'Algo</title>

    <link rel="icon" type="image/png" href="assets/images/favicon-32x32.png" />

    <!-- RESET CSS -->
    <link type="text/css" rel="stylesheet" href="assets/css/reset.css">

    <!-- CSS -->
    <link type="text/css" rel="stylesheet" href="assets/css/prism.css" />
    <link type="text/css" rel="stylesheet" href="assets/css/main.css" />
</head>
<body>
    <div class="container">
        <!-- SIDEBAR: Start -->
        <header class="sidebar">
            <!-- Sidebar: Title -->
            <a class="sidebar__title" href="<?= ROOT; ?>">
                L'algo
            </a>

            <p class="sidebar__intro">Q'est ce que vous chechez ?</p>

            <div class="sidebar__group">
                <?php foreach($categorie->getMainCategories() as $mainCat): ?>
                    <!-- Parent categorie -->
                    <h2 class="title text-<?= $mainCat['color']; ?>"><?= $mainCat['name']; ?></h2>

                    <!-- Sub categorie -->
                    <?php foreach($categorie->getSubCategories($mainCat['cid']) as $subCat): ?>
                        <a class="collaps" href="#"><?= $subCat['name']; ?></a>

                        <!-- Exercice -->
                        <div class="collaps__content">
                            <?php foreach($categorie->getExercicesInCategorie($subCat['cid']) as $exercice): ?>
                                <a 
                                    class="exercice <?= ($exercice['status']) ? "done" : "inwork"; ?>" 
                                    href="?page=exercice&action=show&id=<?= $exercice['id']; ?>"><?= $exercice['name']; ?>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                <?php endforeach; ?>
        </header>
        <!-- SIDEBAR: End -->

        <div class="main">
            <!-- TOP BAR : Start -->
            <div class="topbar">
                <div class="topbar__left">
                    <?php if(isset($_SESSION['uid'])): ?>
                        <a class="btn btn-primary" href="<?= ROOT; ?>?page=exercice&action=create">+ Ajouter un exercice</a>
                        <a class="btn btn-secondary" href="#">+ Créer une catégorie</a>
                    <?php else: ?>
                        <a class="btn btn-secondary" href="https://bitbucket.org/guillian77/l-algo/src/master/" target="_blank"><i class="fab fa-bitbucket"></i> Dépôt</a>
                    <?php endif; ?>
                </div>

                <div class="topbar__right">
                    <?php if(empty($_SESSION['uid'])): ?>
                        <a class="btn btn-primary btn-block" href="?page=user&action=login">Se connecter</a>
                    <?php else: ?>
                        <p class="username">Bonjour <strong><?= $_SESSION['username']; ?></strong></p>
                        <p><a class="btn btn-warning" href="?page=user&action=logout">Se déconnecter</a></p>
                        <p><a class="btn btn-danger ml-1" href="?page=admin&action=index">Administration</a></p>
                    <?php endif; ?>
                </div>
            </div>
            <!-- TOP BAR : End -->

            <?= $content; ?>
        </div>
    </div>
    
    <script src="assets/fonts/fontawesome/js/all.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/prism.js"></script>
    <script src="assets/js/app.js"></script>
</body>
</html>