<!-- BREADCRUMB: Start -->
<nav class="breadcrumb">
    <a class="breadcrumb__item start" href="<?= ROOT; ?>"><i class="fas fa-home"></i></a>
    <span class="breadcrumb__item separator"><i class="fas fa-arrow-right"></i></span>
    <a class="breadcrumb__item">Exercices</a>
    <span class="breadcrumb__item separator"><i class="fas fa-arrow-right"></i></span>
    <a class="breadcrumb__item current"><?= $exercice->name; ?></a>
</nav>
<!-- BREADCRUMB: End -->

<section class="main__content">

    <!-- EXERCICE LINKS: Start  -->
    <div class="exercice_links top">
        <?php if(!empty($prev)): ?>
            <a href="?page=exercice&action=show&id=<?= $prev->id; ?>" title="<?= $prev->name; ?>">← <span>Exercice précédent</span></a>
        <?php endif; ?>
        <?php if(!empty($next)): ?>
            <a href="?page=exercice&action=show&id=<?= $next->id; ?>" title="<?= $next->name; ?>"><span>Exercice suivant</span> →</a>
        <?php endif; ?>
    </div>
    <!-- EXERCICE LINKS: End  -->

    <!-- CONTENT HEADER: Start -->
    <div class="content__header">
        <h2 class="main__title"><?= $exercice->name; ?> <i><?= ($exercice->status === "1") ? "Terminé": "En cours"; ?></i></h2>

        <?php if(isset($_SESSION['uid'])): ?>
            <div class="header__right">
                <?php if($exercice->status === "1"): ?>
                    <a class="btn btn-secondary" href="<?= ROOT; ?>?page=exercice&action=edit&id=<?= $exercice->id; ?>">Éditer cet exercice</a>
                <?php else: ?>
                    <a class="btn btn-primary" href="<?= ROOT; ?>?page=exercice&action=edit&id=<?= $exercice->id; ?>">Répondre</a>
                <?php endif; ?>
                <a class="btn btn-danger" href="#">Supprimer</a>
            </div>
        <?php endif; ?>
    </div>
    <!-- CONTENT HEADER: Start -->

    <div class="exercice">

        <!-- CONSIGNE: Start -->
        <div class="exercice__consigne">
            <h3 class="title title-md text-orange">Consigne</h3>
            <pre class="consigne__text"><?= $exercice->consigne; ?></pre>
        </div>
        <!-- CONSIGNE: End -->

        
        <!-- PSEUDO CODE: Start -->
        <?php if(!empty($exercice->pseudo_code)): ?>
            <div class="exervice__pseudo-code">
                <h3 class="title title-md text-purple">Pseudo code</h3>

                <div class="codeblock">
                    <pre><code class="language-js"><?= $exercice->pseudo_code; ?></code></pre>
                </div>            
            </div>
        <?php endif; ?>
        <!-- PSEUDO CODE: End -->
        
        <!-- RESULT: Start -->
        <?php if(!empty($exercice->result)): ?>
            <div class="exervice__pseudo-code">
                <h3 class="title title-md text-red">Réponse</h3>
                <div class="codeblock">
                    <pre><code class="language-javascript"><?= $exercice->result; ?></code></pre>
                </div>
            </div>
        <?php endif; ?>
        <!-- RESULT: End -->

        <?php if($exercice->javascript || $exercice->jquery || $exercice->php): ?>

            <!-- CODE DISPLAYER: Start -->
            <div class="code-displayer mt-1">
                <h3 class="title title-md text-red">Conversion</h3>

                <div class="codeblock">
                    <pre id="javascript" class="show"><code class="language-javascript"><?= ($exercice->javascript) ? $exercice->javascript : "Pas de code Javascript présent pour cet exercice"; ?></code></pre>

                    <pre id="jquery"><code class="language-javascript"><?= ($exercice->jquery) ? $exercice->jquery : "Pas de code Jquery présent pour cet exercice"; ?></code></pre>

                    <pre id="php"><code class="language-php"><?= ($exercice->php) ? $exercice->php : "Pas de code PHP présent pour cet exercice"; ?></code></pre>
                </div>
            </div>
            <!-- CODE DISPLAYER: End -->

            <!-- CODE SELECTOR: Start -->
            <div class="code-selector mt-1">
                <p>Choisissez un language:</p>
                <?php if($exercice->javascript): ?><button class="btn btn-secondary" data-type="javascript">Javascript</button><?php endif; ?>
                <?php if($exercice->jquery): ?><button class="btn btn-secondary" data-type="jquery">Jquery</button><?php endif; ?>
                <?php if($exercice->php): ?><button class="btn btn-secondary" data-type="php">PHP</button><?php endif; ?>
            </div>
            <!-- CODE SELECTOR: End -->

            <!-- CUSTOMS FIELDS: Start -->
            <?php if($fields): ?>
                <p class="mt-1">Entrez les valeurs nécessaires:</p>
                <form class="input-displayer mt-1" action="#code-result" method="POST">
                    <?php foreach($fields as $key => $field): ?>
                        <label for="<?= $field['field_name']; ?>"><?= $field['field_name']; ?></label>
                        <input class="input input-lg mb-2" type="text" placeholder="<?= $field['placeholder']; ?>" name="<?= $field["field_name"]; ?>" data-script="<?= $field['field_name']; ?>" id="<?= $field['field_name']; ?>" value="<?= (!empty($_POST[$field['field_name']])) ? $_POST[$field['field_name']]: ""; ?>" />
                    <?php endforeach; ?>

                    <input type="hidden" name="send-php-form" value="1">

                    <input class="btn btn-primary mt-1" type="submit" value="Exécuter" />
                </form> 
            <?php endif; ?>
            <!-- CUSTOMS FIELDS: End -->
            
            <!-- CODE RESPONSE DISPLAYER: Start -->
            <div class="response-displayer mt-2">
                <h3 class="title title-md text-red" id="code-result">Résultat du code</h3>

                <p>Le résultat s'affichera après l'exécution du code.</p>

                <div class="codeblock">
                    <pre id="javascript"><code class="language-javascript"></code></pre>
                    <pre id="jquery"><code class="language-jquery"></code></pre>
                    <pre id="php" <?= ($scriptResult) ? 'class="show"' : "" ?>><code class="language-javascript"><?= $scriptResult; ?></code></pre>
                </div>
            </div>
            <!-- CODE RESPONSE DISPLAYER: End -->
        <?php endif; ?>
    </div>

    <!-- EXERCICE LINKS: Start  -->
    <div class="exercice_links bottom">
        <?php if(!empty($prev)): ?>
            <a href="?page=exercice&action=show&id=<?= $prev->id; ?>" title="<?= $prev->name; ?>">← <span>Exercice précédent</span></a>
        <?php endif; ?>
        <?php if(!empty($next)): ?>
            <a href="?page=exercice&action=show&id=<?= $next->id; ?>" title="<?= $next->name; ?>"><span>Exercice suivant</span> →</a>
        <?php endif; ?>
    </div>
    <!-- EXERCICE LINKS: End  -->

</section>