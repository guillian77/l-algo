<!-- BREADCRUMB: Start -->
<nav class="breadcrumb">
    <a class="breadcrumb__item start" href="<?= ROOT; ?>"><i class="fas fa-home"></i></a>
    <span class="breadcrumb__item separator"><i class="fas fa-arrow-right"></i></span>
    <a class="breadcrumb__item">Exercices</a>
    <span class="breadcrumb__item separator"><i class="fas fa-arrow-right"></i></span>
    <a class="breadcrumb__item current">Nouveau</a>
</nav>
<!-- BREADCRUMB: End -->

<!-- Any content after: IN WORK -->
<section class="main__content">
    <div class="content__header">
        <h2 class="main__title">Créer un nouveau travaux</h2>

        <div class="header__right">
            <a class="btn btn-primary" href="<?= ROOT; ?>">Annuler</a>
            <a class="btn btn-primary" href="#">Créer une catégorie</a>
        </div>
    </div>

    <form class="mt-1" action="" method="POST" style="width: 50%;">
        <label for="text">Titre:</label>
        <input class="input input-lg input-block" type="text" placeholder="Titre de l'exercice" name="name" />

        <label class="mt-1" for="consigne">Consigne:</label>
        <textarea class="input input-block" name="consigne" id="consigne" cols="30" rows="4" placeholder="Quel est la consigne pour cet exercice ?"></textarea>

        <label class="mt-1" for="pseudo_code">Pseudo code:</label>
        <textarea class="input input-block" name="pseudo_code" id="pseudo_code" cols="30" rows="4" placeholder="Y a t-il un pseudo code pour cet exercice ?"></textarea>
        
        <label for="main_categorie" class="mt-1">Sélectionner la catégorie principale</label>
        <select name="main_categorie" id="main_categorie">
            <option value="-1" selected="selected"></option>
            <?php foreach($mainCategories as $mainCategorie): ?>
                <option value="<?= $mainCategorie['cid']; ?>"><?= $mainCategorie['name']; ?></option>
            <?php endforeach; ?>
        </select>

        <div class="hidden-select" style="display: none;">
            <label for="sub_categorie" class="mt-1">Sélectionner la sous catégorie</label>
            <select name="sub_categorie" id="sub_categorie">
                <option value="-1" selected="selected"></option>

                <?php foreach($subCategories as $subCategorie): ?>
                    <option value="<?= $subCategorie['cid']; ?>" data-parent="<?= $subCategorie['parent']; ?>"><?= $subCategorie['name']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <label class="checkbox mt-5" for="status">
            <input type="checkbox" name="create_script" value="1" id="status" />
            <span class="checkbox__round"></span>
            <span class="checkbox__label">Créer automatiquement les scripts à partir d'un calque (JS, jQuery et PHP).</span>
        </label>

        <input class="btn btn-primary mt-1" type="submit" value="Envoyer" />
    </form>

</section>