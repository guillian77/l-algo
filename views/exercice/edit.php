<!-- BREADCRUMB: Start -->
<nav class="breadcrumb">
    <a class="breadcrumb__item start" href="<?= ROOT; ?>"><i class="fas fa-home"></i></a>
    <span class="breadcrumb__item separator"><i class="fas fa-arrow-right"></i></span>
    <a class="breadcrumb__item">Exercices</a>
    <span class="breadcrumb__item separator"><i class="fas fa-arrow-right"></i></span>
    <a class="breadcrumb__item current">Éditer</a>
</nav>
<!-- BREADCRUMB: End -->

<!-- Any content after: IN WORK -->
<section class="main__content">
    <div class="content__header">
        <h2 class="main__title">Éditer cet exercice</h2>

        <div class="header__right">
            <a class="btn btn-secondary" href="<?= ROOT; ?>?page=exercice&action=show&id=<?= $exercice->id; ?>">Annuler les modifications</a>
        </div>
    </div>

    <form class="mt-1" action="" method="POST" style="width: 80%;">
        <!-- Input[text]: Title -->
        <label for="text">Titre *:</label>
        <input class="input input-lg input-block" type="text" placeholder="Titre de l'exercice" name="name" value="<?= $exercice->name; ?>" />

        <!-- Textarea: Consigne -->
        <label class="mt-1" for="consigne">Consigne *:</label>
        <textarea class="input input-block" name="consigne" id="consigne" cols="30" rows="4" placeholder="Quel est la consigne pour cet exercice ?"><?= $exercice->consigne; ?></textarea>

        <!-- Textarea: Pseudo code -->
        <label class="mt-1" for="pseudo_code">Pseudo code:</label>
        <textarea class="input input-block" name="pseudo_code" id="pseudo_code" cols="30" rows="4" placeholder="Y a t-il un pseudo code pour cet exercice ?"><?= (!empty($exercice->pseudo_code)) ? $exercice->pseudo_code : ""; ?></textarea>

        <!-- Textarea: Result -->
        <label class="mt-1" for="result">Résultat:</label>
        <textarea class="input input-block" name="result" id="result" cols="30" rows="4" placeholder="Réponse de cet exercice ..."><?= (!empty($exercice->result)) ? $exercice->result : ""; ?></textarea>

        <!-- Button: Add custom fields -->
        <div class="customs-fields">
            <p class="mt-1">Vous pouvez ajouter un champ d'entré pour les exercices qui nécessites des paramètres dynamiques.</p>

            <button class="btn btn-secondary" id="addCustomField"><i class="fas fa-plus-circle"></i> Ajouter un champ personnalisé</button>

            
            <?php foreach($cfields as $key => $cfield): ?>
                <!-- Input name -->
                <input class="input input-lg mt-1 custom-field" type="text" name="customField[<?= $key; ?>][name]" value="<?= $cfield['field_name']; ?>" data-fieldnum="<?= $key; ?>" placeholder="Input name" />

                <!-- Input hidden: for slug  -->
                <input type="hidden" name="customField[<?= $key; ?>][slug]" value="<?= $cfield['slug']; ?>" />

                <!-- Deletion cross -->
                <span class="delete-cross" data-fieldnum="<?= $key; ?>"><i class="far fa-times-circle"></i></span>

                <!-- Input placeholder: for user indications -->
                <input class="input input-lg custom-field placeholder" type="text" name="customField[<?= $key; ?>][placeholder]" placeholder="Input placeholder" value="<?= $cfield['placeholder']; ?>" data-fieldnum="<?= $key; ?>" />
                
            <?php endforeach; ?>
        </div>
        

        <!-- Checkbox: Finish work -->
        <label class="checkbox mt-5" for="status">
            <input type="checkbox" name="status" value="1" id="status" <?= ($exercice->status == 1) ? "checked" : ""; ?> />
            <span class="checkbox__round"></span>
            <span class="checkbox__label">Cocher pour terminer ce travaux</span>
        </label>

        <!-- Input[submit] -->
        <input class="btn btn-primary mt-1" type="submit" value="Sauvegarder les modification" />
    </form>

</section>