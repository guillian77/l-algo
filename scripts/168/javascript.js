function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nStringChain = document.querySelector('input[name="stringChain"]').value;
    let iNbVoy = 0;

    for (let index = 0; index < nStringChain.length; index++) {
        if (!nStringChain[index].search(/[aeiouy]/)) {
            iNbVoy++;
        }
    }

    output.innerHTML = "[JS] Nombre de voyelles: " + iNbVoy;
}
