function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let nStringChain = $('input[name="stringChain"]').val();

    let iNbVoy = 0;

    for (let index = 0; index < nStringChain.length; index++) {
        if (!nStringChain[index].search(/[aeiouy]/)) {
            iNbVoy++;
        }
    }

    $output.html("[jQuery] Nombre de voyelles: " + iNbVoy);
}
