// Already defined by javascript.js
// let count = 0;
// let max = 0;
// let repetition = 20;

function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $input = $('input[name="number"]');

    count++;

    $input.attr("placeholder", "Entrez le nombre " + (count+1));
    $output.html("Entrez le nombre " + (count+1));

    if (count === 1 || $input.val() > max) {
        max = $input.val();
        position = count;
    }

    $input.val("");

    if (count === repetition) {
        $input.remove();
        $output.html("Le nombre le plus grand est " + max + "<br/>C’était le nombre numéro " + position + ".");
    }
}
