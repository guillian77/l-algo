function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $number = $('input[name="number"]').val();
    let textToDisplay = "";

    $number = parseInt($number);

    for (let index = $number; index < ($number + 10); index++) {
        textToDisplay += index + "<br/>";
    }

    $output.html(textToDisplay);

}
