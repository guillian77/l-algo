function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $number = $('input[name="number"]').val();
    let toDisplay = "";

    for (let index = 1; index < 11; index++) {
        toDisplay += $number + " x " + index + " = " + ($number*index) + "<br/>";
    }

    $output.html(toDisplay);
}

