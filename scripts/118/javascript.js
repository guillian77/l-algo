function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let number = document.querySelector('input[name="number"]').value;
    let toDisplay = "";

    for (let index = 1; index < 11; index++) {
        toDisplay += number + " x " + index + " = " + (number*index) + "<br/>";
    }

    output.innerHTML = toDisplay;
}
