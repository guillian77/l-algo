function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')

    let latinVowel = ["a","e","i","u","o","y"];
    
    $output.html(latinVowel);
}
