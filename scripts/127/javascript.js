function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');

    let latinVowel = ["a","e","i","u","o","y"];
    output.innerHTML = latinVowel;
}
