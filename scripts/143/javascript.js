let length = 0;
let loopLap = 0;
let values = [];

function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nEntry = document.querySelector('input[name="entry"]');
    let max = 0;
    let maxIndex = 0;

    if (nEntry.value.length > 0 && length === 0) {
        length = +nEntry.value;
    } else {
        values[loopLap - 1] = +nEntry.value;
    }

    if (loopLap < length) {
        nEntry.placeholder = "Entrez le nombre n°" + (loopLap + 1) + " sur " + length;
        output.innerHTML = "Entrez le nombre n°" + (loopLap + 1) + " sur " + length;
        nEntry.value = "";
    }
    else {
        nEntry.disabled = true;
        nEntry.value = "Travail terminé !";

        max = values[0];
        for (let index = 0; index < values.length; index++) {
            if (values[index] > max) {
                max = values[index];
                maxIndex = index;
            }
        }

        output.innerHTML = "[JS]: La valeur saisie la plus grand est: " + max + ". Elle a été saisie en position " + ( maxIndex + 1) + " sur " + length;
    }

    loopLap++;
}
