// Already defined inside javascript.js
// let length = 0;
// let loopLap = 0;
// let values = [];

function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $nEntry = $('input[name="entry"]');
    let max = 0;
    let maxIndex = 0;

    console.log($nEntry.val())

    if ($nEntry.val().length > 0 && length === 0) {
        length = +$nEntry.val();
    } else {        
        values[loopLap - 1] = +$nEntry.val();
    }

    if (loopLap < length) {
        $nEntry.attr("placeholder", "Entrez le nombre n°" + (loopLap + 1) + " sur " + length);
        $output.html("Entrez le nombre n°" + (loopLap + 1) + " sur " + length);
        $nEntry.val("");
    }
    else {
        $nEntry.prop('disabled', true);
        $nEntry.val("Travail terminé !");

        max = values[0];
        for (let index = 0; index < values.length; index++) {
            if (values[index] > max) {
                max = values[index];
                maxIndex = index;
            }
        }

        $output.html("[jQuery]: La valeur saisie la plus grand est: " + max + ". Elle a été saisie en position " + ( maxIndex + 1) + " sur " + length);
    }
    
    loopLap++;
}
