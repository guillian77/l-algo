<?php

if (empty($_SESSION['length'])) {
    $_SESSION['length'] = 0;
    $_SESSION['values'] = [];
    $_SESSION['loopLap'] = 0;
}

if ($_POST['entry'] > 0 && $_SESSION['length'] === 0) {
    $_SESSION['length'] = $_POST['entry'];
} else {
    $_SESSION['values'][( $_SESSION['loopLap'] - 1 )] = intval($_POST['entry']);
}


if ($_SESSION['loopLap'] < $_SESSION['length']) {
    echo "Entrez le nombre n°" . ( $_SESSION['loopLap'] + 1 ) . " sur " . $_SESSION['length'];
    $_POST['entry'] = "Entrez le nombre n°" . ( $_SESSION['loopLap'] + 1 ) . " sur " . $_SESSION['length'];
} else {
    $_POST['entry'] = "Travail terminé !";
    
    $max = $_SESSION['values'][0];
    $maxIndex = 0;
    foreach ($_SESSION['values'] as $index => $value) {
        if ($value > $max) {
            $max = $value;
            $maxIndex = $index;
        }
    }
    echo "[PHP]: La valeur saisie la plus grand est: " . $max . ". Elle a été saisie en position " . ( $maxIndex + 1) . " sur " . $_SESSION['length'];

    $_SESSION['length'] = 0;
    $_SESSION['posCount'] = 0;
    $_SESSION['negCount'] = 0;
    $_SESSION['loopLap'] = 0;
}

$_SESSION['loopLap']++;
