<?php

/**
 * @var String $filePath Path of address.txt file
 */
$filePath = "scripts/174/address.txt";

/**
 * Format Data
 * String should always have the same length and should be in lower case.
 * @param $data The string to format.
 * @param $length [Optional] Length string should be.
 * @return String The formated string.
 */
function formatData(String $data, $length = 20):String {
    return strtolower( str_pad($data, $length) );
}

$nameToEdit = formatData($_POST['nameToEdit']);

// Open file in writing mode, cursor at the end of file.
$fStream = fopen($filePath, "r");

if ($fStream) {

    $i = 0;
    $users = [];
    $userFound = FALSE;

    while (!feof($fStream)) {
        $line = fgets($fStream);

        if (substr($line, 0, 20) == formatData($nameToEdit)) {
            $userFound = TRUE;
            $line = str_replace(formatData($nameToEdit), formatData($_POST['newName']), $line);
        }

        $users[$i] = $line;

        $i++;
    }

    fclose($fStream);

    if ($userFound) {

        $fStream = fopen($filePath, "w");

        foreach ($users as $user) {
            fwrite($fStream, $user);
        }

        fclose($fStream);

        echo "Le carnet d'adresse à été changé";
    } else {
        echo "Aucun nom correspondant dans le carnet d'adresse.";
    }
}
