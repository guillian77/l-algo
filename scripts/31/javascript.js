function javascript() {
    let nbrArticle = document.querySelector('.input-displayer input[name="nbrArticle"]').value
    let tauxTVA = document.querySelector('.input-displayer input[name="tauxTVA"]').value
    let prixHT = document.querySelector('.input-displayer input[name="prixHT"]').value
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript')

    let displayTtc = function (prixHT, nbrArticle, tauxTVA) {
        var totalHT = (prixHT*nbrArticle)
        return totalHT + (totalHT * (tauxTVA/100))
    }

    
    output.classList.add('show')
    output.innerHTML = displayTtc(prixHT, nbrArticle, tauxTVA)
    
}