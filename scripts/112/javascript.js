function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let day = document.querySelector('input[name="day"]').value;
    let month = document.querySelector('input[name="month"]').value;
    let year = document.querySelector('input[name="year"]').value;

    let daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    day = +day;
    month = +month;
    year = +year;

    /**
     * Add one day to February on leap years
     * Februrary:
     *      - month(input) : 2
     *      - daysInMonths[1] : Array offset
     * "%" get modulo of a division (the rest).
     */
    if (month == 2)
    {
        let rest = year % 4;
        if (rest == 0) { daysInMonths[1] = 29; }
    }

    function checkDate() {
        if (month < 1 || month > 12) { return false; } // Month [1;12]
        if (day < 1 || day > daysInMonths[month -1]) { return false; } // /!\ daysInMonths[month - 1] : Array offset
        if (year < 1) { return false; } // Year should be positive
        return true;
    }

    if (checkDate()) {
        output.innerHTML = "La date est bonne."
    } else {
        output.innerHTML = "La date n'est pas correcte."
    }
}
