<?php

$daysInMonths;
$rest;

$day = intval($_POST['day']);
$month = intval($_POST['month']);
$year = intval($_POST['year']);

function dateCheck($day, $month, $year) {
    $daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    if ($month == 2)
    {
        $rest = $year % 4;
        if ($rest == 0) { $daysInMonths[1] = 29; }
    }

    if ($month < 1 || $month > 12) { return false; } // Month [1;12]
    if ($day < 1 || $day > $daysInMonths[$month -1]) { return false; } // /!\ daysInMonths[month - 1] : Array offset
    if ($year < 1) { return false; } // Year should be positive
    
    return true;
}

if (dateCheck($day, $month, $year)) {
    echo "La date est bonne.";
} else {
    echo "La date n'est pas correcte.";
}
