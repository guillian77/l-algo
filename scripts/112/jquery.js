function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show');
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show');
    let $day = $('input[name="day"]').val();
    let $month = $('input[name="month"]').val();
    let $year = $('input[name="year"]').val();

    let daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    let rest;

    $day = +$day;
    $month = +$month;
    $year = +$year;

    if ($month == 2)
    {
        rest = $year % 4;
        if (rest == 0) { daysInMonths[1] = 29; }
    }

    function checkDate() {
        if ($month < 1 || $month > 12) { return false; } // Month [1;12]
        if ($day < 1 || $day > daysInMonths[$month -1]) { return false; } // /!\ daysInMonths[month - 1] : Array offset
        if ($year < 1) { return false; } // Year should be positive
        return true;
    }

    if (checkDate()) {
        $output.html("La date est bonne.");
    } else {
        $output.html("La date n'est pas correcte.");
    }
}
