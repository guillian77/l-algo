// Already defined by javascript.js
// let count = 0;
// let max = 0;
// let repetition = 20;

function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show');
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show');
    let $input = $('input[name="number"]');
    let number = parseInt($input.val());

    count++;

    $input.attr("placeholder", "Entrez le nombre " + (count+1));
    $output.html("Entrez le nombre " + (count+1));

    if (count === 1 || number > max) {
        max = number;
        position = count;
    }

    $input.val("");

    if (number === 0) {
        $input.remove();
        $output.html("Le nombre le plus grand est " + max + "<br/>C’était le nombre numéro " + position + ".");
    }
}
