let count = 0;
let max = 0;
let position = 0;

function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let input = document.querySelector('input[name="number"]');
    let number = parseInt(input.value);

    count++;

    input.placeholder = "Entrer un nombre " + (count+1);
    output.innerHTML = "Entrer le nombre " + (count+1);

    if (count === 1 || number > max) {
        max = number;
        position = count;
    }

    input.value = ""; // reset input value

    if (number === 0) {
        input.remove();
        output.innerHTML = "Le nombre le plus grand est " + max + "<br/>C’était le nombre numéro " + position + ".";
    }
}
