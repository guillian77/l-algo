<?php

$_POST['number'] = intval($_POST['number']);

if (empty($_SESSION['count'])) {
    $_SESSION['count'] = 0;
    $_SESSION['max'] = 0;
    $_SESSION['repetition'] = 5;
    $_SESSION['position'] = 0;
}

$_SESSION['count']++;

if ($_SESSION['count'] === 1 || $_POST['number'] > $_SESSION['max']) {
    $_SESSION['max'] = $_POST['number'];
    $_SESSION['position'] = $_SESSION['count'];
}

if ($_POST['number'] === 0) {
    echo "Le nombre le plus grand est " . $_SESSION['max'] . "\n C’était le nombre numéro " . $_SESSION['position'] . ".";
    $_SESSION['count'] = 0;
    $_SESSION['max'] = 0;
} else {
    echo "Entrez le nombre " . ($_SESSION['count']+1);
}
