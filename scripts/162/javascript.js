function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    
    let renderTheArray = "";
    let tab = [];
    let max = 0;
    
    /**
     * @return {Number} A random number [0,100]
     */
    let randomize = function() { return Math.floor( ( Math.random() * 100 ) ); }

    /**
     * Init a 12,8 array with random values.
     */
    for (let i = 0; i < 12; i++) {
        
        tab[i] = [];

        for (let j = 0; j < 8; j++) {
            tab[i][j] = randomize();
        }
    }

    /**
     * Look for max value
     */
    max = tab[0][0];
    for (let i = 0; i < tab.length; i++) {
        
        renderTheArray += "[I = " + i + "]: ";

        for (let j = 0; j < tab[i].length; j++) {
            if (tab[i][j] > max) {
                max = tab[i][j];
            }

            renderTheArray += " " + tab[i][j];
        }

            renderTheArray += '<br/>';
    }

    output.innerHTML = "[JS]: La plus grand valeur est: " + max + "<br/>" + renderTheArray;
}
