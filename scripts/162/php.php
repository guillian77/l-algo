<?php

$tab = array();

/**
 * Init a 12,8 array with random values.
 */
for ($i = 0; $i < 12; $i++) {

    for ($j = 0; $j < 8; $j++) {
        $tab[$i][$j] = rand(0,100);
    }
}

/**
 * Look for max value
 */
$max = $tab[0][0];
foreach ($tab as $fDim) {

    foreach ($fDim as $sDim)
    {
        if ($sDim > $max) { $max = $sDim; }
    }

}

print_r("[PHP] La valeur max est: " . $max);
