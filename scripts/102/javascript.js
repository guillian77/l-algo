function javascript() {
    let number = document.querySelector('.input-displayer input[name="number"]').value
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript')
    output.classList.add('show')

    if (number < 0) {
        output.innerHTML = "Le nombre est négatif."
    } else if (number > 0) {
        output.innerHTML = "Le nombre est positif."
    } else {
        output.innerHTML = "Le nombre est 0 !"
    }
}
