function jquery() {
    let $number = $('input[name="number"]').val()
    let $output = $('.response-displayer > .codeblock > pre#javascript').addClass('show')
    
    if($number < 0) {
        $output.html('Le nombre est négatif.')
    } else if ($number > 0) {
        $output.html('Le nombre est positif.')
    } else {
        $output.html('Le nombre est 0.')
    }
}
