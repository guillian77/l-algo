<?php

$intify = intval($_POST['number']);

if ($intify < 0) {
    echo "Le nombre est négatif.";
} else if ($intify >= 1) {
    echo "Le nombre est positif.";
} else if (strlen($_POST['number']) && intval($intify === 0)) {
    echo "Le nombre est 0.";
}