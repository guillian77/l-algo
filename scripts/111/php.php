<?php

$accident = intval($_POST['accident']);
$age = intval($_POST['age']);
$assurance = intval($_POST['assurance']);
$permis = intval($_POST['permis']);
$contrat;

// Tarif vert
if ($age >= 25 && $permis >= 2) {
    if ($accident == 1) {
        $contrat = "orange";
    } else if ($accident > 1) {
        $contrat = "rouge";
    } else {
        $contrat = "vert";
    }
}
// Tarif orange
else if ($age <= 25 && $permis >= 2 && $accident < 1 || $age >= 25 && $permis <= 2 && $accident < 1) {
    $contrat = "orange";
}
// Tarif rouge
else if ($age <= 25 && $permis <= 2 && $accident < 1) {
    $contrat = "rouge";
}
// Refusé
else {
    $contrat = "non accepté";
}

// Contrat fidélité
if ($assurance >= 5) {
    if ($contrat === "vert") { $contrat = "bleu"; }
    else if ($contrat === "orange") { $contrat = "vert"; }
    else if($contrat === "rouge") { $contrat  = "orange"; }
}

echo "Le client a un contrat $contrat";