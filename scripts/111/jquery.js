function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show');
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show');
    let $accident = $('input[name="accident"]').val();
    let $age = $('input[name="age"]').val();
    let $assurance = $('input[name="assurance"]').val();
    let $permis = $('input[name="permis"]').val();

    let contrat;

    // Tarif vert
    if ($age >= 25 && $permis >= 2) {
        if ($accident == 1) {
            contrat = "orange";
        } else if ($accident > 1) {
            contrat = "rouge";
        } else {
            contrat = "vert";
        }
    }
    // Tarif orange
    else if ($age <= 25 && $permis >= 2 && $accident < 1 || $age >= 25 && $permis <= 2 && $accident < 1) {
        contrat = "orange";
    }
    // Tarif rouge
    else if ($age <= 25 && $permis <= 2 && $accident < 1) {
        contrat = "rouge";
    }
    // Refusé
    else {
        contrat = "non accepté"
    }

    // Contrat fidélité
    if ($assurance >= 5) {
        if (contrat === "vert") { contrat = "bleu"; }
        else if (contrat === "orange") { contrat = "vert"; }
        else if(contrat === "rouge") { contrat  = "orange"; }
    }

    $output.html("Le client a un contrat " + contrat);
}
