function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let accident = document.querySelector('input[name="accident"]').value; // Le nombre d'accident
    let age = document.querySelector('input[name="age"]').value; // L'age du conducteur
    let assurance = document.querySelector('input[name="assurance"]').value; // Nombre d'années au sein de l'assurance
    let permis = document.querySelector('input[name="permis"]').value; // Le nombre d'années en possesion du permis

    let contrat;

    // Tarif vert
    if (age >= 25 && permis >= 2) {
        if (accident == 1) {
            contrat = "orange";
        } else if (accident > 1) {
            contrat = "rouge";
        } else {
            contrat = "vert";
        }
    }
    // Tarif orange
    else if (age <= 25 && permis >= 2 && accident < 1 || age >= 25 && permis <= 2 && accident < 1) {
        contrat = "orange";
    }
    // Tarif rouge
    else if (age <= 25 && permis <= 2 && accident < 1) {
        contrat = "rouge";
    }
    // Refusé
    else {
        contrat = "non accepté"
    }

    // Contrat fidélité
    if (assurance >= 5) {
        if (contrat === "vert") { contrat = "bleu"; }
        else if (contrat === "orange") { contrat = "vert"; }
        else if(contrat === "rouge") { contrat  = "orange"; }
    }

    output.innerHTML = "Le client a un contrat " + contrat;
} 
