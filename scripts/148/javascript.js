let loopLap = 0;
let values = [];

function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nValue = document.querySelector('input[name="value"]');
    let consecutive = true;
    let prevValue = 0;

    values[loopLap] = +nValue.value;
    nValue.value = "";

    for (let index = 1; index < values.length; index++) {
        prevValue = ( ( values[index - 1] ) + 1 );
        
        if (prevValue != values[index]) {
            consecutive = false;
        }
    }

    loopLap++;

    output.innerHTML = "[JS]: Valeurs consécutives: " + consecutive;
}
