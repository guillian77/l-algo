// Already definded inside javascript.js
// let loopLap = 0;
// let values = [];

function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $nValue = $('input[name="value"]');
    let consecutive = true;
    let prevValue = 0;

    values[loopLap] = +$nValue.val();
    $nValue.val("");

    for (let index = 1; index < values.length; index++) {
        prevValue = ( ( values[index - 1] ) + 1 );
        
        if (prevValue != values[index]) {
            consecutive = false;
        }
    }

    loopLap++;

    $output.html("[jQuery]: Valeurs consécutives: " + consecutive);
}
