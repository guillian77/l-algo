<?php

if (empty($_SESSION['loopLap71'])) {
    $_SESSION['loopLap71'] = 0;
    $_SESSION['values71'] = [];
}

$_SESSION['values71'][$_SESSION['loopLap71']] = intval($_POST['value']);
unset($_POST['value']);

$consecutive = TRUE;

foreach ($_SESSION['values71'] as $index => $currValue) {
    if ($index > 0) {
        $prevValue = ($_SESSION['values71'][$index - 1] + 1);

        if ($prevValue != $currValue) {
            $consecutive = FALSE;
        }
    }
}

$_SESSION['loopLap71']++;

var_dump($consecutive);
var_dump($_SESSION['values71']);
