<?php

$values = [1, 2, 3, 4, 5, 6, "a", "b", "c"];
$temp = [];
$index = 0;

/**
 * Get last array key inside an array
 * @param $array An array.
 */
function getArrayLastIndex(Array $array) {
    $lastKey;

    foreach ($array as $key => $value) {
        $lastKey = $key;
    }

    return $lastKey;
}

for ($revIndex = getArrayLastIndex($values); $revIndex >= 0 ; $revIndex--) { 
    $temp[$index] = $values[$revIndex];
    $index++;
}

$values = $temp;

print_r($values);
