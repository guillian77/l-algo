function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    
    let values = [1, 2, 3, 4, 5, 6, "a", "b", "c"];
    let temp = [];

    let index = 0;
    for (let revIndex = values.length-1; revIndex >= 0; revIndex--) {
        temp[index] = values[revIndex];
        index++;
    }

    values = temp;

    output.innerHTML = "[JS]: " + values;
}
