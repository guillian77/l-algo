function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let word = $('input[name="word"]').val();

    $output.html("[jQuery]: Le mot " + word + " compte " + word.length + " lettres.");
}
