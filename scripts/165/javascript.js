function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let word = document.querySelector('input[name="word"]').value;

    output.innerHTML = "[JS]: Le mot " + word + " compte " + word.length + " lettres.";
}
