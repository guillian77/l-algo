<?php

if (empty($_SESSION['loopLap'])) {
    $_SESSION['loopLap'] = 0;
    $_SESSION['marks'] = [];
}

$_SESSION['loopLap']++;

$_SESSION['marks'][$_SESSION['loopLap']] = intval($_POST['mark']);
unset($_POST['mark']);

if ($_SESSION['loopLap'] < 10) {
    echo "Ajoutez la note " . $_SESSION['loopLap'];
} else {
    var_dump($_SESSION['marks']);
}
