let loopLap = 0;
let marks = [];

function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nMark = document.querySelector('input[name="mark"]');

    loopLap++;

    if (loopLap < 10)
    {
        marks[loopLap-1] = +nMark.value;

        nMark.value = ""
        nMark.placeholder = "Ajoutez la note " + loopLap;
        output.innerHTML = "Ajoutez la note " + loopLap;

    }
    else
    {
        output.innerHTML = marks;
    }
}
