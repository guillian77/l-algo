// Already defined inside javascript.js
// let loopLap = 0;
// let marks = [];

function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $nMark = $('input[name="mark"]');
    
    loopLap++;

    marks[loopLap-1] = +$nMark.val();
    $nMark.val("");

    if (loopLap < 10)
    {
        $nMark.attr('placeholder', "Ajoutez la note " + loopLap);
        $output.html("Ajoutez la note " + loopLap);
    }
    else
    {
        $nMark.remove();
        $output.html(marks);
    }
}
