<?php

$values = [1, 2, 3, 4, 5, 6, "a", "b", "c"];
$length = count($values);

for ($index=0; $index < $length; $index++) { 
    $posMin = $index;

    for ($nextIndex = $index + 1; $nextIndex < $length; $nextIndex++) { 
        if ($values[$nextIndex] > $values[$posMin]) {
            $posMin = $nextIndex;
        }
    }

    $temp = $values[$posMin];
    $values[$posMin] = $values[$index];
    $values[$index] = $temp;
}

var_dump($values);
