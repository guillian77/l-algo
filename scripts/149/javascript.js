function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let values = [1, 2, 3, 4, 5, 6, "a", "b", "c"];

    for (let index = 0; index < values.length; index++)
    {
        // index is temp the minest index
        // for every lap.
        let posMin = index;
    
        for (let nextIndex = index + 1; nextIndex < values.length; nextIndex++)
        {
            // If (value + 1) > current
            if (values[nextIndex] > values[posMin]) {
                posMin = nextIndex;
            }
        }
    
        // Swap positions
        let temp = values[posMin];
        values[posMin] = values[index];
        values[index] = temp;
    }

    output.innerHTML = "[JS]: " + values;
}
