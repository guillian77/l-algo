function javascript()
{
    /**
     * Just for the ouput
     */
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript')
    output.classList.add('show')

    /**
     * Code
     */
    var val = 231;
    output.innerHTML = (val * 2)
}
