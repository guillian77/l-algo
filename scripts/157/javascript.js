function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
        let toDisplay = "";
        let tab1 = [];

        for (let i = 0; i < 6; i++) {
            tab1[i] = [];
            for (let index = 0; index < 13 ; index++) { 
                tab1[i][index] = 0;
            }
        }

        toDisplay = "";
        for (let i = 0; i < tab1.length; i++) {
            for (let index = 0; index < tab1[i].length; index++) {
                toDisplay += tab1[i][index] + " ";
            }
            toDisplay += "<br/>";
        }

        output.innerHTML = "[JS]:<br/>" + toDisplay;
}
