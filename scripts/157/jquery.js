function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let toDisplay = "";
        let tab1 = [];

        for (let i = 0; i < 6; i++) {
            tab1[i] = [];
            for (let index = 0; index < 13 ; index++) { 
                tab1[i][index] = 0;
            }
        }

        toDisplay = "";
        for (let i = 0; i < tab1.length; i++) {
            for (let index = 0; index < tab1[i].length; index++) {
                toDisplay += tab1[i][index] + " ";
            }
            toDisplay += "<br/>";
        }

        $output.html("[jQuery]:<br/>" + toDisplay);
}
