function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $nIndiceToDelete = $('input[name="indiceToDelete"]');

    let values = [1, 2, 3, 4, 5, 6, "a", "b", "c"];

    /**
     * My Splice
     * Delete an element inside an array.
     * @param {Array} arrToSplice Array to splice.
     * @param {Number} spliceIndex Element index to delete.
     * @return {Array} New array without selected index.
     */
    function mySplice(arrToSplice, spliceIndex) {
        let temp = [];

        for (let index = 0; index < arrToSplice.length-1; index++) {
            temp[index] = arrToSplice[index];
    
            if (index >= spliceIndex) {
                temp[index] = arrToSplice[index + 1];
            }
        }

        return temp;
    }

    // Replace old, by new array content.
    values = mySplice(values, $nIndiceToDelete.val());

    $output.html("[jQuery]: " + values);
}
