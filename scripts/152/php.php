<?php

$values = [1, 2, 3, 4, 5, 6, "a", "b", "c"];

/**
 * My Splice
 * Delete an element inside an array.
 * @param Array $arrToSplice Array to splice.
 * @param Int $spliceIndex Element index to delete.
 * @return Array New array without selected index.
 */
function mySplice(Array $arrToSplice, Int $spliceIndex):Array {
    $temp = [];
    $length = count($arrToSplice);

    for ($index = 0; $index < $length-1; $index++) {
        $temp[$index] = $arrToSplice[$index];

        if ($index >= $spliceIndex) {
            $temp[$index] = $arrToSplice[$index + 1];
        }
    }

    return $temp;
}

print_r(mySplice($values, $_POST['indiceToDelete']));
