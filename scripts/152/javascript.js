function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nIndiceToDelete = document.querySelector('input[name="indiceToDelete"]');
    let values = [1, 2, 3, 4, 5, 6, "a", "b", "c"];

    /**
     * My Splice
     * Delete an element inside an array.
     * @param {Array} arrToSplice Array to splice.
     * @param {Number} spliceIndex Element index to delete.
     * @return {Array} New array without selected index.
     */
    function mySplice(arrToSplice, spliceIndex) {
        let temp = [];

        for (let index = 0; index < arrToSplice.length-1; index++) {
            temp[index] = arrToSplice[index];
    
            if (index >= spliceIndex) {
                temp[index] = arrToSplice[index + 1];
            }
        }

        return temp;
    }

    // Replace old, by new array content.
    values = mySplice(values, nIndiceToDelete.value);

    output.innerHTML = "[JS]: " + values;
}
