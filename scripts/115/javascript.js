let iNombreHasard = Math.floor((Math.random() * 100));

function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let number = document.querySelector('input[name="number"]').value;

    if (number < iNombreHasard) {
        output.innerHTML = "Trop petit !";
    } else if (number > iNombreHasard) {
        output.innerHTML = "Trop grand !";
    } else {
        output.innerHTML = "Bravo !";
    }
}
