// let iNombreHasard = Math.floor((Math.random() * 100));

function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $number = $('input[name="number"]').val();

    if ($number < iNombreHasard) {
        $output.html("Trop petit !");
    } else if ($number > iNombreHasard) {
        $output.html("Trop grand !");
    } else {
        $output.html("Bravo !");
    }
}
