<?php

if (empty($_SESSION['iNombreHasard'])) {
    $_SESSION['iNombreHasard'] = rand(1,100);
}

if ($_POST['number'] < $_SESSION['iNombreHasard']) {
    echo "Trop petit !";
} else if ($_POST['number'] > $_SESSION['iNombreHasard']) {
    echo "Trop grand !";
} else {
    echo "Bravo !";
    unset($_SESSION['iNombreHasard']);
}