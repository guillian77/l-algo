<?php

$tab1 = [0, 4, 5, 6, 7, 8, 10, 11];
$tab2 = [1, 2, 3, 9, 12, 99];
$mergedTab = [];

$firstEnded = false; // flag for first array
$secEnded = false; // flag for second array

$firstIndex = 0; // index cursor for first array
$secIndex = 0; // index cursor for second array

$loop = 0;

while (!$firstEnded || !$secEnded) {

    if ($firstEnded || $tab1[$firstIndex] > $tab2[$secIndex]) {

        $mergedTab[$loop] = $tab2[$secIndex];

        $secIndex++;

        if ($secIndex === count($tab2)) { $secEnded = TRUE; }
    }
    else {
        $mergedTab[$loop] = $tab1[$firstIndex];

        $firstIndex++;

        if ($firstIndex === count($tab1)) { $firstEnded = TRUE; }
    }

    $loop++;
}

print_r($mergedTab);
