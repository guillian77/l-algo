<?php

$hours = intval($_POST['hours']);
$minutes = intval($_POST['minutes']);

$minutes++;

if ($minutes === 60) {
    $minutes = 0;
    $hours++;
}

if ($hours === 24) {
    $hours = 0;
}

// Set minutes more cut
if ($hours < 10) { $hours = "0" . $hours; }
if ($minutes < 10) { $minutes = "0" . $minutes; }

echo "Dans une minute, il sera " . $hours . ":" . $minutes;