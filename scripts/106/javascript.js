function javascript() {
    let hours = document.querySelector('.input-displayer input[name="hours"]').value
    let minutes = document.querySelector('.input-displayer input[name="minutes"]').value
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript')
    output.classList.add('show')

    hours = parseInt(hours);
    minutes = parseInt(minutes);

    minutes++;

    if (minutes === 60) {
        minutes = 0;
        hours++;
    }

    if (hours === 24) {
        hours = 0;
    }

    // A little bit style :D
    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }

    output.innerHTML = "Dans une minute, il sera " + hours + ":" + minutes;
}
