function jquery() {
    let $hours = $('.input-displayer input[name="hours"]').val();
    let $minutes = $('.input-displayer input[name="minutes"]').val();
    let $output = $('.response-displayer > .codeblock > pre#javascript');
    $output.addClass('show');

    $hours = parseInt($hours);
    $minutes = parseInt($minutes);

    $minutes++;

    if ($minutes === 60) {
        $minutes = 0;
        $hours++;
    }

    if ($hours === 24) {
        $hours = 0;
    }

    // A little bit style :D
    if ($hours < 10) { $hours = "0" + $hours; }
    if ($minutes < 10) { $minutes = "0" + $minutes; }

    $output.html("Dans une minute, il sera " + $hours + ":" + $minutes);
}
