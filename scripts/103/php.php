<?php

if ($_POST['number_1'] < 0 && $_POST['number_2'] < 0 || $_POST['number_1'] > 0 && $_POST['number_2'] > 0) {
    echo "Le produit des deux nombres est positif";
}
else if($_POST['number_1'] == 0 || $_POST['number_2'] == 0) {
    echo "Le produit vaut 0.";
}
else {
    echo "Le produit des deux nombres est négatif.";
}
