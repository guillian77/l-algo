function javascript() {
    let nbr1 = document.querySelector('input[name="number_1"]').value
    let nbr2 = document.querySelector('input[name="number_2"]').value
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript')
    output.classList.add('show')

    if (nbr1 < 0 && nbr2 < 0 || nbr1 > 0 && nbr2 > 0) {
        output.innerHTML = "Le produit des deux nombres est positif"
    }
    else if(nbr1 == 0 || nbr2 == 0) {
        output.innerHTML = "Le produit vaut 0."
    }
    else {
        output.innerHTML = "Le produit des deux nombres est négatif."
    }
}
