function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $nbr1 = $('input[name="number_1"]').val();
    let $nbr2 = $('input[name="number_2"]').val();

    if ($nbr1 < 0 && $nbr2 < 0 || $nbr1 > 0 && $nbr2 > 0) {
        $output.html("Le produit des deux nombres est positif")
    }
    else if($nbr1 == 0 || $nbr2 == 0) {
        $output.html("Le produit vaut 0.")
    }
    else {
        $output.html("Le produit des deux nombres est négatif.")
    }
}
