function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show');
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show');
    let $nbr_copie = $('input[name="nbr_copie"]').val();
    let cost = 0;

    for (let i = 0; i < $nbr_copie; i++) {
        if(i <= 10) {
            cost = cost + parseFloat('0.10');
        } else if (i <= 30) {
            cost = cost + parseFloat('0.09');
        } else {
            cost = cost + parseFloat('0.08');
        }
    }

    $output.html(cost.toFixed(2) + "€ pour " + $nbr_copie + " copie(s).")
}
