function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nbr_copie = document.querySelector('input[name="nbr_copie"]').value;
    let cost = 0;

    for (let i = 0; i < nbr_copie; i++) {
        if(i <= 10) {
            cost = cost + parseFloat('0.10');
        } else if (i <= 30) {
            cost = cost + parseFloat('0.09');
        } else {
            cost = cost + parseFloat('0.08');
        }
    }

    output.innerHTML = cost.toFixed(2) + "€ pour " + nbr_copie + " copie(s).";
}
