<?php

$nbr_copie = intval($_POST['nbr_copie']);
$cost = 0;

for ($i = 0; $i < $nbr_copie; $i++) {
    if($i <= 10) {
        $cost = $cost + floatval('0.10');
    } else if ($i <= 30) {
        $cost = $cost + floatval('0.09');
    } else {
        $cost = $cost + floatval('0.08');
    }
}

echo round(floatval($cost), 2) . "€ pour " . $nbr_copie . " copie(s).";