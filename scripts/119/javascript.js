function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let number = document.querySelector('input[name="number"]').value;
    let cached = 0;

    for (let index = 1; index < +number + 1; index++)
    {
        cached = cached + index;
    }

    output.innerHTML = cached;
}
