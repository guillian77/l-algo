function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let candidat_1 = document.querySelector('input[name="candidat_1"]').value;
    let candidat_2 = document.querySelector('input[name="candidat_2"]').value;
    let candidat_3 = document.querySelector('input[name="candidat_3"]').value;
    let candidat_4 = document.querySelector('input[name="candidat_4"]').value;


    if(candidat_1 > 50)
    {
        output.innerHTML = 'Le candidat n°1 est élu au premier tour';
    }
    else
    {
        if (candidat_1 < 12.5 || candidat_2 > 50 || candidat_3 > 50 || candidat_4 > 50) {
            output.innerHTML = 'Les candidat n°1 est battu.'
        }
        else if(candidat_1 > candidat_2 && candidat_1 > candidat_3 && candidat_1 > candidat_4) {
            output.innerHTML = "Le candidat n°1 est en ballotage favorable."
        }
        else {
            output.innerHTML = "Le candidat n°1 est en ballotage defavorable."
        }
    }
}
