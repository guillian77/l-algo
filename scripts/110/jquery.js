function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show');
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show');
    let $candidat_1 = $('input[name="candidat_1"]').val();
    let $candidat_2 = $('input[name="candidat_2"]').val();
    let $candidat_3 = $('input[name="candidat_3"]').val();
    let $candidat_4 = $('input[name="candidat_4"]').val();

    if($candidat_1 > 50)
    {
        $output.html('Le candidat n°1 est élu au premier tour');
    }
    else
    {
        if ($candidat_1 < 12.5 || $candidat_2 > 50 || $candidat_3 > 50 || $candidat_4 > 50) {
            $output.html('Les candidat n°1 est battu.');
        }
        else if($candidat_1 > $candidat_2 && $candidat_1 > $candidat_3 && $candidat_1 > $candidat_4) {
            $output.html("Le candidat n°1 est en ballotage favorable.");
        }
        else {
            $output.html("Le candidat n°1 est en ballotage defavorable.");
        }
    }
}
