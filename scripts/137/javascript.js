let count = 0;
let sum = 0;

function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nNote = document.querySelector('input[name="cNote"]');

    count++;

    sum = sum + +nNote.value;

    nNote.placeholder = "Entrez la note n°" + ( count + 1 );
    nNote.value = "";

    output.innerHTML = "La moyenne de(s) " + count + " note(s) est actuellement de " + (sum / count);
}
