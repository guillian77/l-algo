<?php

/**
 * @var Integer Max length of a string
 */
$stringWidth = 20;

// Complete strings white spaces
$name = str_pad($_POST['name'], $stringWidth);
$firstname = str_pad($_POST['firstname'], $stringWidth);
$tel = str_pad($_POST['tel'], $stringWidth);
$mail = str_pad($_POST['mail'], $stringWidth);

// $concatenated = "\n" . $name . $firstname . $tel . $mail;
$concatenated = PHP_EOL . $name . $firstname . $tel . $mail;

// Open file in writing mode, cursor at the end of file.
$fStream = fopen("scripts/172/address.txt", "a");

if ($fStream) {
    fwrite($fStream, $concatenated);
    fclose($fStream);
    echo "One row has been added to address.txt";
}
