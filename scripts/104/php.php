<?php

$groups = [
    'poussin' => [6,7],
    'pupille' => [8,9],
    'minime' => [10,11],
    'cadet' => [12,999]
];

foreach ($groups as $key => $groupName) {
    if (in_array($_POST['age_enfant'], $groupName)) {
        echo "L'enfant se trouve dans la catégorie " . $key;
    }
}
