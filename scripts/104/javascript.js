function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let age = document.querySelector('input[name="age_enfant"]').value;
    
    var groups = [];
        groups['poussin'] = 6;
        groups['pupille'] = 8;
        groups['minime'] = 10;
        groups['cadet'] = 12;

    for(key in groups) {
        if (age >= groups[key]) {
            output.innerHTML = key
        }
    }
}
