function jquery() {
    let $age = $('input[name="age_enfant"]').val();
    $('.response-displayer > .codeblock > pre').removeClass('show');
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show');

    const groups = {
        poussin : [6,7],
        pupille : [8,9],
        minime : [10,11],
        cadet : [12,999],
    };

    $.each(groups, function(key, value) {
        if (value.includes(parseInt($age))) {
            $output.html("L'enfant se trouve dans la catégorie " + key);
        }
    });
}
