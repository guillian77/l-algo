let count = 0;
let max = 0;
let repetition = 20; // for testing

function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let input = document.querySelector('input[name="number"]');

    count++;

    input.placeholder = "Entrer un nombre " + (count+1);
    output.innerHTML = "Entrer le nombre " + (count+1);

    if (count == 1 || input.value > max) {
        max = input.value;
    }

    input.value = ""; // reset input value

    if (count === repetition) {
        input.remove();
        output.innerHTML = "Le nombre le plus grand est " + max;
    }
}
