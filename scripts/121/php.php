<?php

if (empty($_SESSION['count'])) {
    $_SESSION['count'] = 0;
    $_SESSION['max'] = 0;
    $_SESSION['repetition'] = 5;
}

$_SESSION['count']++;

if ($_SESSION['count'] === 1 || $_POST['number'] > $_SESSION['max']) {
    $_SESSION['max'] = $_POST['number'];
}

if ($_SESSION['count'] === $_SESSION['repetition']) {
    echo "Le nombre le plus grand est " . $_SESSION['max'];
    $_SESSION['count'] = 0;
    $_SESSION['max'] = 0;
} else {
    echo "Entrez le nombre " . ($_SESSION['count']+1);
}
