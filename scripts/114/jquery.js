function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $nombre = $('input[name="nombre"]').val();

    if ($nombre < 10) {
        $output.html("Plus grand !");
    } else if ($nombre > 20) {
        $output.html("Plus petit !");
    } else {
        $output.html("Bravo !");
    }
}
