function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');

    // METHODE DEMANDÉE:
    // let nombre = prompt("Veuillez saisir un nombre entre un 10 et 20.");
    // while(nombre < 10 || nombre > 20)
    // {
    //     if (nombre < 10) {
    //         nombre = prompt("Plus grand !");
    //     }
    //     else {
    //         nombre = prompt("Plus petit !");
    //     }
    // }
    // alert ("Bravo !");

    let nombre = document.querySelector('input[name="nombre"]').value;

    if (nombre < 10) {
        output.innerHTML = "Plus grand !";
    } else if (nombre > 20) {
        output.innerHTML = "Plus petit !";
    } else {
        output.innerHTML = "Bravo !";
    }
}
