let length = 0;
let loopLap = 0;
let values = [];

function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nEntry = document.querySelector('input[name="entry"]');
    let toDisplay = "";

    if (nEntry.value.length > 0 && length === 0) {
        length = +nEntry.value;
    } else {
        values[loopLap - 1] = ( +nEntry.value + 1 );
    }

    if (loopLap < length) {
        nEntry.placeholder = "Entrez le nombre n°" + (loopLap + 1) + " sur " + length;
        output.innerHTML = "Entrez le nombre n°" + (loopLap + 1) + " sur " + length;
        nEntry.value = "";
    }
    else {
        nEntry.disabled = true;
        nEntry.value = "Travail terminé !";

        for(index in values) {
            toDisplay += values[index] + "<br/>";
        }

        output.innerHTML = "[JS]: Les valeurs saisies + 1 sont:<br/>" + toDisplay;
    }

    loopLap++;
}
