<?php

if (empty($_SESSION['length'])) {
    $_SESSION['length'] = 0;
    $_SESSION['values'] = [];
    $_SESSION['loopLap'] = 0;
}

if ($_POST['entry'] > 0 && $_SESSION['length'] === 0) {
    $_SESSION['length'] = $_POST['entry'];
} else {
    $_SESSION['values'][( $_SESSION['loopLap'] - 1 )] = intval(( $_POST['entry'] + 1 ));
}


if ($_SESSION['loopLap'] < $_SESSION['length']) {
    echo "Entrez le nombre n°" . ( $_SESSION['loopLap'] + 1 ) . " sur " . $_SESSION['length'];
    $_POST['entry'] = "Entrez le nombre n°" . ( $_SESSION['loopLap'] + 1 ) . " sur " . $_SESSION['length'];
} else {
    $_POST['entry'] = "Travail terminé !";

    $toDisplay = "";
    
    foreach ($_SESSION['values'] as $value) {
        $toDisplay .= $value . "\n";
    }

    echo "[PHP]:  Les valeurs saisies + 1 sont:\n" . $toDisplay;

    $_SESSION['length'] = 0;
    $_SESSION['posCount'] = 0;
    $_SESSION['negCount'] = 0;
    $_SESSION['loopLap'] = 0;
}

$_SESSION['loopLap']++;
