// Already defined inside javascript.js
// let length = 0;
// let loopLap = 0;
// let values = [];

function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $nEntry = $('input[name="entry"]');
    let toDisplay = "";

    console.log($nEntry.val())

    if ($nEntry.val().length > 0 && length === 0) {
        length = +$nEntry.val();
    } else {        
        values[loopLap - 1] = ( +$nEntry.val() + 1 );
    }

    if (loopLap < length) {
        $nEntry.attr("placeholder", "Entrez le nombre n°" + (loopLap + 1) + " sur " + length);
        $output.html("Entrez le nombre n°" + (loopLap + 1) + " sur " + length);
        $nEntry.val("");
    }
    else {
        $nEntry.prop('disabled', true);
        $nEntry.val("Travail terminé !");

        for(index in values) {
            toDisplay += values[index] + "<br/>";
        }

        $output.html("[jQuery]: Les valeurs saisies + 1 sont:<br/>" + toDisplay);
    }
    
    loopLap++;
}
