<?php

if (empty($_SESSION['sum'])) {
    $_SESSION['sum'] = 0;
    $_SESSION['rest'] = 0;
    $_SESSION['payed'] = 0;
}

$_SESSION['sum'] = $_SESSION['sum'] + intval($_POST['article_price']);
$_SESSION['payed'] = $_SESSION['payed'] + intval($_POST['payed']);
$_SESSION['rest'] = $_SESSION['sum'] - $_SESSION['payed'];
$nb10eTicket = 0;
$nb5eTicket = 0;
$nb1ePiece = 0;
$toDisplay = "";

while ($_SESSION['rest'] <= -10) {
    $nb10eTicket++;
    $_SESSION['rest'] = ($_SESSION['rest'] + 10);
}

while ($_SESSION['rest'] <= -5) {
    $nb5eTicket++;
    $_SESSION['rest'] = ($_SESSION['rest'] + 5);
}

while ($_SESSION['rest'] <= -1) {
    $nb1ePiece++;
    $_SESSION['rest'] = ($_SESSION['rest'] + 1);
}

$toDisplay .= "Vous avez acheté pour un total de " . $_SESSION['sum'] . "€ \n";
$toDisplay .= "Vous avez payé " . $_SESSION['payed'] . "€ \n";

if ($_SESSION['rest'] > 0) {
    $toDisplay .= "Vous devez encore " . $_SESSION['rest'] . "€ \n";
} else {
    $toDisplay .= "Je vous rend: \n";
    $toDisplay .= "    " . $nb10eTicket . " billet(s) de 10€ \n";
    $toDisplay .= "    " . $nb5eTicket . " billet(s) de 5€ \n";
    $toDisplay .= "    " . $nb1ePiece . " pièce(s) de 1€";

    $_SESSION['sum'] = 0;
    $_SESSION['rest'] = 0;
    $_SESSION['payed'] = 0;
}

echo $toDisplay;

unset($_POST['article_price']);
unset($_POST['payed']);
