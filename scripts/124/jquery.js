// Already define inside javascript.js
// let sum = 0;
// let rest = 0;
// let payed = 0;

function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $nPrice = $('input[name="article_price"]');
    let $nPayed = $('input[name="payed"]');
    let nb10eTicket = 0;
    let nb5eTicket = 0;
    let nb1ePiece = 0;
    let toDisplay = "";

    sum = sum + +$nPrice.val();
    payed = payed + +$nPayed.val();
    rest = sum - payed;

    while(rest <= -10) {
        nb10eTicket++;
        rest = (rest + 10);
    }

    while(rest <= -5) {
        nb5eTicket++;
        rest = (rest + 5);
    }

    while(rest <= -1) {
        nb1ePiece++;
        rest = (rest + 1);
    }

    toDisplay += "Vous avez acheté pour un total de " + sum + "€ <br/>";
    toDisplay += "Vous avez payé " + payed + "€ <br/>";

    if (rest > 0) {
        toDisplay += "Vous devez encore " + rest + "€ <br/>";
    } else {
        toDisplay += "Je vous rend: <br/>";
        toDisplay += "    " + nb10eTicket + " billet(s) de 10€ <br/>";
        toDisplay += "    " + nb5eTicket + " billet(s) de 5€ <br/>";
        toDisplay += "    " + nb1ePiece + " pièce(s) de 1€";
    }

    $output.html(toDisplay);

    // Reset fields values
    $nPrice.val("") 
    $nPayed.val("")
}
