let sum = 0;
let rest = 0;
let payed = 0;

function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nPrice = document.querySelector('input[name="article_price"]');
    let nPayed = document.querySelector('input[name="payed"]');
    let nb10eTicket = 0;
    let nb5eTicket = 0;
    let nb1ePiece = 0;
    let toDisplay = "";

    sum = sum + +nPrice.value;
    payed = payed + +nPayed.value
    rest = sum - payed;

    while(rest <= -10) {
        nb10eTicket++;
        rest = (rest + 10);
    }

    while(rest <= -5) {
        nb5eTicket++;
        rest = (rest + 5);
    }

    while(rest <= -1) {
        nb1ePiece++;
        rest = (rest + 1);
    }

    toDisplay += "Vous avez acheté pour un total de " + sum + "€ <br/>";
    toDisplay += "Vous avez payé " + payed + "€ <br/>";
    
    if (rest > 0) {
        toDisplay += "Vous devez encore " + rest + "€ <br/>";
    } else {
        toDisplay += "Je vous rend: <br/>";
        toDisplay += "    " + nb10eTicket + " billet(s) de 10€ <br/>";
        toDisplay += "    " + nb5eTicket + " billet(s) de 5€ <br/>";
        toDisplay += "    " + nb1ePiece + " pièce(s) de 1€";
    }

    output.innerHTML = toDisplay;

    // Reset fields values
    nPrice.value = "";
    nPayed.value = ""

}
