function javascript()
{
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript')
    output.classList.add('show')
    var block = String

    let p1 = "belle marquise"
    let p2 = "vos beaux yeux"
    let p3 = "me font mourir"
    let p4 = "d’amour"

    block  = "• " + p1 + " " + p2 + " " + p3 + " " + p4 + "\n"
    block += "• " + p3 + " " + p2 + " " + p1 + " " + p4 + "\n"
    block += "• " + p2 + " " + p3 + " " + p1 + " " + p4 + "\n"
    block += "• " + p4 + " " + p1 + " " + p2 + " " + p3 + "\n"

    output.innerHTML = block
}
