function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nString = document.querySelector('input[name="string"]').value;
    let toDisplay = "";

    // Parse string with spaces and count array index
    toDisplay += "Split: " + ( nString.split(" ").length-1 ) + "<br/>";

    /**
     * \s Find spaces
     * /g Global, find all reccurences of needed
     */
    toDisplay += "Match: " + nString.match(/\s/g).length;

    output.innerHTML = "[JS]: " + toDisplay;
}
