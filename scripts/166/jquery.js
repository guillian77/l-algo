function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let nString = $('input[name="string"]').val();
    let toDisplay = "";

    // Parse string with spaces and count array index
    toDisplay += "Split: " + ( nString.split(" ").length-1 ) + "<br/>";

    /**
     * \s Find spaces
     * /g Global, find all reccurences of needed
     */
    toDisplay += "Match: " + nString.match(/\s/g).length;

    $output.html("[jQuery]: " + toDisplay);
}
