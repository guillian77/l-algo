function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let aIntegers = [];
    let consecutive = true;

    // Create integers array
    for (let index = 0; index < 100; index++) {
        aIntegers[index] = index + 1;
    }

    // Look for any error
    for (let index = 1; index < aIntegers.length; index++) {
        if (aIntegers[index] != ( aIntegers[index -1] + 1 ) )
        {
            // If previous index != (current + 1)
            consecutive = false;
        }
    }

    $output.html("[jQuery]: " + consecutive);
}
