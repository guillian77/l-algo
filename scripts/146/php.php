<?php

$aIntegers = [];
$consecutive = TRUE;

// Create integers array
for ($index = 0; $index < 100; $index++) {
    $aIntegers[$index] = $index + 1;
}

// Look for any error
for ($index = 1; $index < $aIntegers; $index++) {
    if ($aIntegers[$index] != ( $aIntegers[$index -1] + 1 ) )
    {
        // If previous index != (current + 1)
        $consecutive = FALSE;
    }
}

echo "[PHP]: " . $consecutive;
