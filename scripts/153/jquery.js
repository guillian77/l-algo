async function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $nSearchedWord = $('input[name="searched"]');

    // Just load an array of french words list from JSON file. And AWAIT IT !
    let dictionnary = await $.getJSON("scripts/153/francais.json", function(json) {
        return json;
    });

    let start = 0;
    let middle;
    let end = ( dictionnary.length - 1 );
    let wordFound = false;

    while (wordFound != true && start <= end)
    {
        // Divide dictionnary array by 2
        middle = Math.floor( ( start + end ) / 2 );

        if (dictionnary[middle] == $nSearchedWord.val()) {
            wordFound = true;
        }
        else if (dictionnary[middle] > $nSearchedWord.val()) {
            // First half of the array [0 to (arrayLength // 2)]
            end = middle - 1;
        }
        else {
            // Second half of the array [(arrayLength // 2) to arrayLength]
            start = middle + 1;
        }
    }

    $output.html('[jQuery]: Le mot "' + $nSearchedWord.val() + '" est inconnu dans le dictionnaire.');
    $nSearchedWord.attr("placeholder", "Cherchez un mot existant dans le dictionnaire");
    $nSearchedWord.css("border", "2px solid red")

    if (wordFound) {
        $output.html('[jQuery]: Le mot "' + $nSearchedWord.val() + '" a été trouvé dans le dictionnaire.');
        $nSearchedWord.attr("placeholder", "Cherchez un autre mot dans le dictionnaire");
        $nSearchedWord.css("border", "2px solid green")
    }

    $nSearchedWord.val("");
}
