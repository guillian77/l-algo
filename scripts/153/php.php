<?php
// Load my dictionnary from a file (JSON)
$dictionnary = file_get_contents("scripts/153/francais.json");
$dictionnary = json_decode($dictionnary);

$start = 0;
$middle;
$end = ( count($dictionnary) - 1 );
$wordFound = false;

while($wordFound != true && $start <= $end) {
    $middle = floor( ( $start + $end ) / 2 );

    if ($dictionnary[$middle] === $_POST['searched']) {
        $wordFound = true;
    }
    elseif ($dictionnary[$middle] > $_POST['searched']) {
        $end = $middle - 1;
    } else {
        $start = $middle + 1;
    }
}

if ($wordFound) {
    echo "Trouvé";
} else {
    echo "Inconnu";
}

$_POST['searched'] = "";
