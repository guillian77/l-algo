async function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nSearchedWord = document.querySelector('input[name="searched"]');

    // Just load an array of french words list from JSON file. And AWAIT IT !
    let dictionnary = await $.getJSON("scripts/153/francais.json", function(json) {
        return json;
    });

    let start = 0;
    let middle;
    let end = ( dictionnary.length - 1 );
    let wordFound = false;

    while (wordFound != true && start <= end)
    {
        // Divide dictionnary array by 2
        middle = Math.floor( ( start + end ) / 2 );

        if (dictionnary[middle] == nSearchedWord.value) {
            wordFound = true;
        }
        else if (dictionnary[middle] > nSearchedWord.value) {
            // First half of the array [0 to (arrayLength // 2)]
            end = middle - 1;
        }
        else {
            // Second half of the array [(arrayLength // 2) to arrayLength]
            start = middle + 1;
        }
    }

    output.innerHTML = '[JS]: Le mot "' + nSearchedWord.value + '" est inconnu dans le dictionnaire.';
    nSearchedWord.placeholder = "Cherchez un mot existant dans le dictionnaire";
    nSearchedWord.style.border = "2px solid red";

    if (wordFound) {
        output.innerHTML = '[JS]: Le mot "' + nSearchedWord.value + '" a été trouvé dans le dictionnaire.';
        nSearchedWord.placeholder = "Cherchez un autre mot dans le dictionnaire";
        nSearchedWord.style.border = "2px solid green";
    }

    nSearchedWord.value = "";
}
