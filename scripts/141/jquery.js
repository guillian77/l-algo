function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')

    let tab1 = [4,8,7,12];
    let tab2 = [3,6];
    let toon = 0;

    for (iT2 in tab2)
    {        
        for (let iT1 = 0; iT1 < tab1.length; iT1++)
        {
             toon = ( toon + ( tab2[iT2] * tab1[iT1] ) );
        }
    }

    $output.html("[jQuery]: Le toon vaut " + toon);
}
