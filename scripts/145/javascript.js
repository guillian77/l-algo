function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nAge = document.querySelector('input[name="age"]');
    let sum = 1000;

    for (let index = 0; index < +nAge.value; index++) {
        sum = sum + ( sum * 2.75 / 100 );
    }

    output.innerHTML = "[JS]: La somme présente sur le compte est de: " + sum + " euros";

    if (nAge.value < 1 || nAge.value > 20) {
        nAge.value = "";
        nAge.placeholder = "L'age doit être compris entre 1 et 20 ans, re-essayez s'il vous plaît.";
        output.innerHTML = "[JS]: L'age doit être compris entre 1 et 20 ans, re-essayez s'il vous plaît.";
    }
}
