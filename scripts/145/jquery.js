function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $nAge = $('input[name="age"]');
    let sum = 1000;

    for (let index = 0; index < +$nAge.val(); index++) {
        sum = sum + ( sum * 2.75 / 100 );
    }

    $output.html("[jQuery]: La somme présente sur le compte est de: " + sum + " euros");

    if ($nAge.val() < 1 || $nAge.val() > 20) {
        $nAge.val("");
        $nAge.attr("placeholder", "L'age doit être compris entre 1 et 20 ans, re-essayez s'il vous plaît.");
        $output.html("[jQuery]: L'age doit être compris entre 1 et 20 ans, re-essayez s'il vous plaît.");
    }
}
