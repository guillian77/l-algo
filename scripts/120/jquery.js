function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $number = $('input[name="number"]').val();
    let cached = 1;

    for (let index = 1; index < +$number + 1; index++)
    {
        cached = cached * index;
    }

    $output.html(cached);
}
