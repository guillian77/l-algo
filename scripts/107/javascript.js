function javascript() {
    let hours = document.querySelector('.input-displayer input[name="hours"]').value
    let minutes = document.querySelector('.input-displayer input[name="minutes"]').value
    let seconds = document.querySelector('.input-displayer input[name="seconds"]').value
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript')
    output.classList.add('show')

    hours = parseInt(hours);
    minutes = parseInt(minutes);
    seconds = parseInt(seconds);

    seconds++;

    if (seconds === 60) {
        seconds = 0;
        minutes++;
    }

    if (minutes === 60) {
        minutes = 0;
        hours++;
    }

    if (hours === 24) {
        hours = 0;
    }

    // A little bit style :D
    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }

    output.innerHTML = "Dans une seconde, il sera " + hours + ":" + minutes + ":" + seconds;
}
