<?php

$hours = intval($_POST['hours']);
$minutes = intval($_POST['minutes']);
$seconds = intval($_POST['seconds']);

$seconds++;

if ($seconds === 60) {
    $seconds = 0;
    $minutes++;
}

if ($minutes === 60) {
    $minutes = 0;
    $hours++;
}

if ($hours === 24) {
    $hours = 0;
}

// Set minutes more cut
if ($hours < 10) { $hours = "0" . $hours; }
if ($minutes < 10) { $minutes = "0" . $minutes; }
if ($seconds < 10) { $seconds = "0" . $seconds; }

echo "Dans une seconde, il sera " . $hours . ":" . $minutes . ":" . $seconds;