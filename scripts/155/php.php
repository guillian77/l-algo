<?php

prixTotalTTC($_POST['prixHT'], $_POST['nbrArticle'], $_POST['tauxTVA']);

function prixTotalTTC ($prixHT, $nbrArticle, $tauxTVA) {
    $totalHT = $prixHT*$nbrArticle;
    $totalTTC = $totalHT + ($totalHT * ($tauxTVA/100));

    // Output
    echo "Le prix TTC de {$nbrArticle} article(s) à {$prixHT}€/u avec un taux de {$tauxTVA}% est de {$totalTTC}€";
}