function javascript() {
    // Get value inside input
    let inputValue = document.querySelector('.input-displayer input[name="number"]').value

    // Get output block
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript')
    var response
    
    if (inputValue < 0) {
        response = "Ce nombre est négatif."
    } else if (inputValue > 0) {
        response = "Ce nombre est positif."
    } else {
        response = "&nbsp;"
    }

    // Show output block
    output.classList.add('show')

    // Insert data inside output block
    output.innerHTML = response
}
