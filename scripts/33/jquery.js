function jquery() {
    let $inputValue = $('input[name="number"]:first').val()
    let $output = $('.response-displayer > .codeblock > pre#javascript').addClass('show')
    
    if ($inputValue < 0) {
        $output.html("Ce nombre est négatif.")
    } else if ($inputValue > 0) {
        $output.html("Ce nombre est positif.")
    }
}
