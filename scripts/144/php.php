<?php

if (empty($_SESSION['sum67'])) {
    $_SESSION['sum67'] = 0;
    $_SESSION['count67'] = 0;
    $_SESSION['hitgherThanAverage'] = 0;
}

$_SESSION['count67']++;

$_SESSION['sum67'] = $_SESSION['sum67'] + intval($_POST['cNote']);
$average = ( $_SESSION['sum67'] / $_SESSION['count67'] );

$hitgherThanAverage = 0;
if ($_POST['cNote'] > $average) {
    $_SESSION['hitgherThanAverage']++;
}

unset($_POST['cNote']);

echo "[PHP]: La moyenne de(s) " . $_SESSION['count67'] . " note(s) est actuellement de " . $average . "\nLe nombre de note(s) au dessus de la moyenne est de: " . $_SESSION['hitgherThanAverage'];
