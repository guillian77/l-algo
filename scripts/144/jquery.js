// Already defined inside javascript.js
// let count = 0;
// let sum = 0;
// let marks = [];
// let hitgherThanAverage = 0;

function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $nNote = $('input[name="cNote"]');
    let average = 0;

    count++;

    sum = sum + +$nNote.val();
    average = ( sum / count );

    if (+$nNote.val() > average) {
        hitgherThanAverage++;
    }

    $nNote.attr("placeholder", "Entrez la note n°" + ( count + 1 ));
    $nNote.val("");

    $output.html("[jQuery]: La moyenne de(s) " + count + " note(s) est actuellement de " + average + "<br/>Le nombre de note(s) au dessus de la moyenne est de: " + hitgherThanAverage);
}
