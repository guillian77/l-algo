let count = 0;
let sum = 0;
let marks = [];
let hitgherThanAverage = 0;

function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nNote = document.querySelector('input[name="cNote"]');
    let average = 0;

    count++;

    sum = sum + +nNote.value;
    average = ( sum / count );

    if (+nNote.value > average) {
        hitgherThanAverage++;
    }

    nNote.placeholder = "Entrez la note n°" + ( count + 1 );
    nNote.value = "";

    output.innerHTML = "[JS]: La moyenne de(s) " + count + " note(s) est actuellement de " + average + "<br/>Le nombre de note(s) au dessus de la moyenne est de: " + hitgherThanAverage;
}
