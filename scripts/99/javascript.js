function javascript()
{
    let nbr1 = document.querySelector('.input-displayer input[name="number_1"]').value;
    let nbr2 = document.querySelector('.input-displayer input[name="number_2"]').value;
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
    output.classList.add('show');

    if (nbr1 > 0 && nbr2 > 0 || nbr1 < 0 && nbr2 < 0) {
        output.innerHTML = "Le produit est positif.";
    } else {
        output.innerHTML = "Le produit est négatif.";
    }
}
