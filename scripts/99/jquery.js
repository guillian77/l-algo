function jquery() {
    let $nbr1 = $('input[name="number_1"]').val()
    let $nbr2 = $('input[name="number_2"]').val()
    let $output = $('.response-displayer > .codeblock > pre#javascript').addClass('show')

    if ($nbr1 > 0 && $nbr2 > 0 || $nbr1 < 0 && $nbr2 < 0) {
        $output.html('Le produit est positif.')
    } else {
        $output.html('Le produit est négatif.')
    }
}
