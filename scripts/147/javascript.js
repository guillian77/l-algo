let loopLap = 0;
let values = [];

function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nValue = document.querySelector('input[name="value"]');

    if (loopLap < 100) {
        values[loopLap] = +nValue.value;
        nValue.value = "";
        nValue.placeholder = "Entrez la valeur n°" + (loopLap + 2);

        for (let index = 0; index < values.length; index++)
        {
            // index is temp the minest index
            // for every lap.
            let posMin = index;
        
            for (let nextIndex = index + 1; nextIndex < values.length; nextIndex++)
            {
                // If (value + 1) < current
                if (values[nextIndex] < values[posMin]) {
                    posMin = nextIndex;
                }
            }
        
            // Swap positions
            let temp = values[posMin];
            values[posMin] = values[index];
            values[index] = temp;
        }

        output.innerHTML = "[JS]: " + values;

        loopLap++;
    } else {
        output.innerHTML = "Plus de 100!";
    }    
}
