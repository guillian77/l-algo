// Already defined inside javascript.js
// let loopLap = 0;
// let values = [];

function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $nValue = $('input[name="value"]');
    
    if (loopLap < 100) {
        values[loopLap] = +$nValue.val();
        $nValue.val("");
        $nValue.attr("placeholder", "Entrez la valeur n°" + (loopLap + 2));

        for (let index = 0; index < values.length; index++)
        {
            // index is temp the minest index
            // for every lap.
            let posMin = index;
        
            for (let nextIndex = index + 1; nextIndex < values.length; nextIndex++)
            {
                // If (value + 1) < current
                if (values[nextIndex] < values[posMin]) {
                    posMin = nextIndex;
                }
            }
        
            // Swap positions
            let temp = values[posMin];
            values[posMin] = values[index];
            values[index] = temp;
        }

        $output.html("[jQuery]" + values);

        loopLap++;
    } else {
        $output.html("Plus de 100!");
    }
}
