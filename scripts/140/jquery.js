function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')

    let tab1 = [4,8,7,9,1,5,4,6];
    let tab2 = [7,6,5,2,1,3,7,4];
    let tabToMake = [];
    let sumTab = "";

    for (let index = 0; index < tab1.length; index++) {
        tabToMake[index] = (tab1[index] + tab2[index]);
        sumTab += tabToMake[index] + "<br/>";
    }

    $output.html("[jQuery]:<br/>" + sumTab);
}
