function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');

    let tab1 = [4,8,7,9,1,5,4,6];
    let tab2 = [7,6,5,2,1,3,7,4];
    let sumTab = [];
    let toDisplay = "";

    for (let index = 0; index < tab1.length; index++) {
        sumTab[index] = (tab1[index] + tab2[index]);
        toDisplay += sumTab[index] + "<br/>";
    }

    output.innerHTML = "[JS]:<br/>" + toDisplay;
}
