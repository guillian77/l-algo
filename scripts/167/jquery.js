function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let nStringChain = $('input[name="stringChain"]').val();
    let iNbVoy = 0;
    
    for (let index = 0; index < nStringChain.length; index++) {
        let sCurr = nStringChain[index];
        if (sCurr === "a" || sCurr === "e" || sCurr === "i" || sCurr === "o" || sCurr === "u" || sCurr === "y")
        {
            iNbVoy = ( iNbVoy + 1 );
        }
    }

    $output.html("[jQuery]: Il y a " + iNbVoy + " voyelle(s).");
}
