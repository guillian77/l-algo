function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nStringChain = document.querySelector('input[name="stringChain"]').value;
    let iNbVoy = 0;
    
    for (let index = 0; index < nStringChain.length; index++) {
        let sCurr = nStringChain[index];
        if (sCurr === "a" || sCurr === "e" || sCurr === "i" || sCurr === "o" || sCurr === "u" || sCurr === "y")
        {
            iNbVoy = ( iNbVoy + 1 );
        }
    }

    output.innerHTML = "[JS]: Il y a " + iNbVoy + " voyelle(s).";
}
