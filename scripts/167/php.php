<?php

$stringChain = $_POST['stringChain'];
$iNbVoy = 0;
for ($i=0; $i < strlen($stringChain); $i++) { 
    if(
        $stringChain[$i] === "a" || 
        $stringChain[$i] === "e" || 
        $stringChain[$i] === "i" || 
        $stringChain[$i] === "o" || 
        $stringChain[$i] === "u" || 
        $stringChain[$i] === "y"
        ) {
        $iNbVoy++;
    }
}

echo "[PHP]: Il y a " . $iNbVoy . " voyelle(s).";
