let token = "selection";

function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nCellSelection = document.querySelector('input[name="cellSelection"]').value;

    function buildLayer(height, width, cellStyle = " X ") {
        let layer = [];

        for (let xAxis = 0; xAxis < height; xAxis++) {
            layer[xAxis] = [];

            for (let yAxis = 0; yAxis < width; yAxis++) {
                layer[xAxis][yAxis] = cellStyle;
            }
        }

        return layer;
    }

    function render(layer) {
        let render = "";

        console.log(layer)

        for (let i = 0; i < layer.length; i++) {

            for (let j = 0; j < layer[i].length; j++) {
                render += layer[i][j];
            }

            render += "<br/>"
        }

        return render;
    }

    let layer = buildLayer(16,16);
    layer [2][4] = " O "
    layer = render(layer);

    if (token === "selection") {
        output.innerHTML = layer;
    }

}