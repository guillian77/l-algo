function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let values = [10,12,64,41,22,66,8,80];
    let sum = 0;
    
    for (let index = 0; index < values.length; index++) {
        sum = sum + values[index];        
    }

    $output.html("[jQuery]: La somme des valeurs du tableau est de " + sum);
}
