function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let values = [10,12,64,41,22,66,8,80];
    let sum = 0;

    for (let index = 0; index < values.length; index++) {
        sum = sum + values[index];
    }

    output.innerHTML = "[JS]: La somme des valeurs du tableau est de " + sum;
}
