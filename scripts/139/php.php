<?php

$values = [10,12,64,41,22,66,8,80];
$sum = 0;

foreach ($values as $value) {
    $sum = $sum + $value;
}

echo "[PHP]: La somme des valeurs du tableau est de " . $sum;
