function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $number = $('input[name="number"]').val();

    let textToDisplay = "";
    let index = +$number;

    while (index < +$number + 10) {
        textToDisplay += index + "<br/>";
        index++;
    }

    $output.html(textToDisplay);
}
