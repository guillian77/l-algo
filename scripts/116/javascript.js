function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let number = document.querySelector('input[name="number"]').value;

    let textToDisplay = "";
    let index = +number;

    while (index < +number + 10) {
        textToDisplay += index + "<br/>";
        index++;
    }

    output.innerHTML = textToDisplay;
}
