function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $chiffre = $('input[name="chiffre"]').val();

    if ($chiffre < 1 || $chiffre > 3) {
        $output.html("Ressaisissez un chiffre entre un 1 et 3.");
    } else {
        $output.html("Bravo !");
    }
}
