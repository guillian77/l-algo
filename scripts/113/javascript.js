function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');

    // METHODE DEMANDÉE:
    // chiffre = prompt("Veuillez saisir un chiffre entre un 1 et 3.");
    // while(chiffre < 1 || chiffre > 3)
    // {
    //     chiffre = prompt("Ressaisissez un chiffre entre un 1 et 3.");
    // }
    // alert ("Bravo !");

    let chiffre = document.querySelector('input[name="chiffre"]');

    if (chiffre.value < 1 || chiffre.value > 3) {
        output.innerHTML = "Ressaisissez un chiffre entre un 1 et 3.";
        chiffre.value = "";
    } else {
        output.innerHTML = "Bravo !";
    }
}
