let length = 0;
let posCount = 0;
let negCount = 0;
let loopLap = 0;

function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    let nEntry = document.querySelector('input[name="entry"]');

    if (nEntry.value.length > 0 && length === 0) {
        length = +nEntry.value;
    } else {        
        if (nEntry.value > 0) { posCount++; }
        else { negCount++; }
    }

    nEntry.placeholder = "Entrez le nombre n°" + (loopLap + 1) + " sur " + length;
    output.innerHTML = "Entrez le nombre n°" + (loopLap + 1) + " sur " + length;

    if (loopLap < length) { nEntry.value = ""; }
    else {
        nEntry.disabled = true;
        nEntry.value = "Travail terminé !";
        output.innerHTML = "[JS]: Valeurs positive(s): " + posCount + "<br/>Valeur(s) négative(s): " + negCount;
    }

    loopLap++;
}
