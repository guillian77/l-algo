// Already defined inside javascript.js
// let length = 0;
// let posCount = 0;
// let negCount = 0;
// let loopLap = 0;

function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $nEntry = $('input[name="entry"]');

    console.log($nEntry.val())

    if ($nEntry.val().length > 0 && length === 0) {
        length = +$nEntry.val();
    } else {        
        if ($nEntry.val() > 0) { posCount++; }
        else { negCount++; }
    }

    $nEntry.attr("placeholder", "Entrez le nombre n°" + (loopLap + 1) + " sur " + length);
    $output.html("Entrez le nombre n°" + (loopLap + 1) + " sur " + length);

    if (loopLap < length) { $nEntry.val(""); }
    else {
        $nEntry.prop('disabled', true);
        $nEntry.val("Travail terminé !");
        $output.html("[jQuery]: Valeurs positive(s): " + posCount + "<br/>Valeur(s) négative(s): " + negCount);
    }
    
    loopLap++;
}
