<?php

if (empty($_SESSION['length'])) {
    $_SESSION['length'] = 0;
    $_SESSION['posCount'] = 0;
    $_SESSION['negCount'] = 0;
    $_SESSION['loopLap'] = 0;
}

if ($_POST['entry'] > 0 && $_SESSION['length'] === 0) {
    $_SESSION['length'] = $_POST['entry'];
} else {
    if ($_POST['entry'] > 0) {
        $_SESSION['posCount']++;
    } else {
        $_SESSION['negCount']++;
    }
}


if ($_SESSION['loopLap'] < $_SESSION['length']) {
    echo "Entrez le nombre n°" . ( $_SESSION['loopLap'] + 1 ) . " sur " . $_SESSION['length'];
    $_POST['entry'] = "Entrez le nombre n°" . ( $_SESSION['loopLap'] + 1 ) . " sur " . $_SESSION['length'];
} else {
    $_POST['entry'] = "Travail terminé !";
    echo "[PHP]: Valeurs positive(s): " . $_SESSION['posCount'] . "\nValeur(s) négative(s): " . $_SESSION['negCount'];

    $_SESSION['length'] = 0;
    $_SESSION['posCount'] = 0;
    $_SESSION['negCount'] = 0;
    $_SESSION['loopLap'] = 0;
}

$_SESSION['loopLap']++;
