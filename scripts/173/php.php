<?php

/**
 * @var String $filePath Path of address.txt file
 */
$filePath = "scripts/173/address.txt";

/**
 * Format Data
 * String should always have the same length and should be in lower case.
 * @param $data The string to format.
 * @param $length [Optional] Length string should be.
 * @return String The formated string.
 */
function formatData(String $data, $length = 20):String {
    return strtolower( str_pad($data, $length) );
}

$concatenated = formatData($_POST['firstname']);
$concatenated .= formatData($_POST['name']);
$concatenated .= formatData($_POST['tel']);
$concatenated .= formatData($_POST['mail']);
$concatenated .= PHP_EOL;

// Open file in writing mode, cursor at the end of file.
$fStream = fopen($filePath, "r");

if ($fStream) {

    $i = 0;
    $users = [];

    while (!feof($fStream)) {
        $users[$i] = fgets($fStream);
        $i++;
    }

    if (!in_array($concatenated, $users)) {
        array_push($users, $concatenated);
        sort($users);

        fclose($fStream);

        $fStream = fopen($filePath, "w");

        foreach ($users as $user) {
            fwrite($fStream, $user);
        }

        fclose($fStream);

        echo $_POST['firstname'] . " a été ajouté au carnet d'adresses.";
    } else {
        echo $_POST['firstname'] . " est déjà dans le carnet d'adresses !";
    }
}
