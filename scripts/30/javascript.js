function javascript() {
    // Get value inside input
    let number = document.querySelector('.input-displayer input[name="number"]').value

    // Get output block
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript')

    function double (number) {
        return Math.pow(number, 2)
    }

    // Show output block
    output.classList.add('show')

    // Insert data inside output block
    output.innerHTML = double(number)
    
}
