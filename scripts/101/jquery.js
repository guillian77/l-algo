function jquery() {
    let $names = $('input[type="text"]')
    let $output = $('.response-displayer > .codeblock > pre#javascript')
        .addClass('show')
        .html("Les prénoms sont dans l'ordre alphbétique.")

    $names.each(function($i) {
        if ($i > 0) {
            if ($names.eq($i-1).val() > $(this).val()) {
                $output.html("Les prénoms ne sont pas dans l'ordre alphbétique.");
            }    
        }
    })

}
