function javascript() {
    let names = document.querySelectorAll('.input-displayer input[type="text"]')
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript')
    output.classList.add('show')

    output.innerHTML = "Les prénoms sont dans l'ordre alphbétique."
    
    names.forEach(function(name, i) {
        if (i > 0) {
            if (names[i-1].value > name.value) {
                output.innerHTML = "Les prénoms ne sont pas dans l'ordre alphbétique."
            }
        }
    })
}
