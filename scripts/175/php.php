<?php

/**
 * @var String $filePath Path of address.txt file
 */
$filePath = "scripts/175/address.txt";

// Open file in writing mode, cursor at the end of file.
$fStream = fopen($filePath, "r");

if ($fStream) {

    $i = 0;
    $users = [];
    $badMailCount = 0;

    while (!feof($fStream)) {
        $line = fgets($fStream);

        $mail = str_replace(" ", "", substr($line, 60, 20) );

        if ( !filter_var($mail, FILTER_VALIDATE_EMAIL) ) {
            $badMailCount++;
        } else {
            $users[$i] = $line;
        }

        $i++;
    }

    fclose($fStream);

    if ($badMailCount > 0) {
        $fStream = fopen($filePath, "w");

        foreach ($users as $user) {
            fwrite($fStream, $user);
        }

        echo $badMailCount . " utilisateur(s) supprimé(s) du carnet d'adresse (mauvais email).";
    } else {
        echo "Les adresses mails de tous les utilisateurs semblent correctes.";
    }
}
