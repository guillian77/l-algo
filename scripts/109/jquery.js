function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')
    let $age = $('input[name="age"]').val();
    let $sex = $('input[name="sexe"]').val();
    
    if($sex === "M" && $age > 20 || $sex === "F" && $age >= 18 && $age < 35) {
        $output.html("Vous êtes imposable :(");
    } else {
        $output.html("Vous n'êtes pas imposable, GG !");
    }
}
