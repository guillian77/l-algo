<?php

$values = [1, 2, 3, 4, 5, 6, "a", "b", "c"];
$yapermute = true;

while ($yapermute) {
    $yapermute = FALSE;

    foreach ($values as $index => $currValue) {
        if ($currValue < $values[$index + 1]) {
            $temp = $currValue;
            $currValue = $values[$index + 1];
            $values[$index + 1] = $temp;

            $yapermute = TRUE;
        }
    }
}