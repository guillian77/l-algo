function jquery() {
    $('.response-displayer > .codeblock > pre').removeClass('show')
    let $output = $('.response-displayer > .codeblock > pre#jquery').addClass('show')

    let values = [1, 2, 3, 4, 5, 6, "a", "b", "c"];
    let yapermute = true;
    
    while (yapermute) {
        yapermute = false;

        for (let index = 0; index < values.length; index++) {
            if (values[index] < values[index + 1]) {
                let temp = values[index];
                values[index] = values[index + 1];
                values[index + 1] = temp;
                yapermute = true;
            }
        }
    }

    $output.html(values);
}
