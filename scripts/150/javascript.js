function javascript() {
    let output = document.querySelector('.response-displayer > .codeblock > pre#javascript');
        output.classList.add('show');
    
    let values = [1, 2, 3, 4, 5, 6, "a", "b", "c"];
    let yapermute = true;

    while (yapermute) {
        yapermute = false;

        for (let index = 0; index < values.length; index++)
        {
            if (values[index] < values[index + 1])
            {
                let temp = values[index];
                values[index] = values[index + 1];
                values[index + 1] = temp;
                yapermute = true;
            }
        }
    } 

    output.innerHTML = values;
}
