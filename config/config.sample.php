<?php

trait Configuration {

    /**
     * @var String $mode [dev | prod]
     * 
     * prod: Application continue to work without error displaying.
     * dev: Display any error, like PDO error.
     * 
     * Pay attention to dev mode and informations that can be displayed.
     */
    public $mode = "dev";

    /**
     * @var Bool $allow_registering Allow or not registering.
     */
    public $allow_registering = FALSE;

    /**
     * @var Array $dbInfo Connexion informations to remote database.
     */
    private $dbInfo = array(
        "name" => 'DatabaseName',
        "host" => 'localhost',
        "user" => 'Username',
        "password" => 'Password',
        "port" => '3306'
    );
}