SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE `algo_categories` (
  `cid` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent` int(12) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `algo_categories` (`cid`, `name`, `parent`, `color`) VALUES
(1, 'Exercices', NULL, 'orange'),
(2, 'Travaux pratiques', NULL, 'purple'),
(3, 'Saison 1', 1, NULL),
(4, 'Saison 2', 1, NULL),
(5, 'Formulaires: Généralités', 2, NULL),
(6, 'Formulaires: BT', 2, NULL),
(7, 'Useless', NULL, 'red'),
(8, 'Saison 3', 1, NULL);

CREATE TABLE `algo_exercices` (
  `id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `categorie` int(12) NOT NULL,
  `consigne` longtext NOT NULL,
  `pseudo_code` varchar(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `algo_exercices` (`id`, `name`, `categorie`, `consigne`, `pseudo_code`, `result`, `status`) VALUES
(1, 'Exercice 1.1', 3, 'Quelles seront les valeurs des variables A et B apr&egrave;s ex&eacute;cution des instructions suivantes ?', 'VARIABLES A, B en ENTIER\r\nDEBUT\r\n    A &larr; 17\r\n    B &larr; A + 2\r\n    A &larr; 9\r\nFIN', 'A vaut 9\r\nB vaut 19', 1),
(2, 'Exercice 1.2', 3, 'Quelles seront les valeurs des variables A, B et C apr&egrave;s ex&eacute;cution des instructions suivantes ?', 'VARIABLES A, B, C en ENTIER\r\nDEBUT\r\n    A &larr; 51\r\n    B &larr; 34\r\n    C &larr; A + B\r\n    A &larr; 21\r\n    C &larr; B &ndash; A \r\nFIN', 'A vaut  21\r\nB vaut  34\r\nC vaut -20', 1),
(3, 'Exercice 1.3', 3, 'Quelles seront les valeurs des variables A et B apr&egrave;s ex&eacute;cution des instructions suivantes ?', 'VARIABLES A, B en ENTIER\r\nDEBUT\r\n    A &larr; 49\r\n    B &larr; A + 4\r\n    A &larr; A + 1\r\n    B &larr; A &ndash; 4\r\nFIN', 'A vaut 50\r\nB vaut 46', 1),
(4, 'Exercice 1.4', 3, 'Quelles seront les valeurs des variables A, B et C apr&egrave;s ex&eacute;cution des instructions suivantes ?', 'VARIABLES A, B, C en ENTIER\r\nDEBUT\r\n    A &larr; 13\r\n    B &larr; 19\r\n    C &larr; A + B\r\n    B &larr; A + B\r\n    A &larr; C\r\nFIN', 'A vaut 32\r\nB vaut 32\r\nC vaut 32', 1),
(5, 'Exercice 1.5', 3, 'Quelles seront les valeurs des variables A et B apr&egrave;s ex&eacute;cution des instructions suivantes ?', 'VARIABLES A, B en ENTIER\r\nDEBUT\r\n    A &larr; 14\r\n    B &larr; 29\r\n    A &larr; B\r\n    B &larr; A\r\nFIN', 'A vaut 29\r\nB vaut 29', 1),
(6, 'Exercice 1.6', 3, 'Ecrire un pseudo-code permettant d&rsquo;&eacute;changer les valeurs de deux variables A et B, et ce quel que soit leur contenu pr&eacute;alable.', 'VARIABLES A, B en ENTIER\r\nDEBUT\r\n    A &larr; 15\r\n    B &larr; 16\r\n    C &larr; A\r\n\r\n    A &larr; B\r\n    B &larr; C\r\nFIN', 'A vaut 16\r\nB vaut 15', 1),
(7, 'Exercice 1.7', 3, 'Une variante du pr&eacute;c&eacute;dent : on dispose de trois variables A, B et C. Ecrivez un algorithme transf&eacute;rant &agrave; B la valeur de A, &agrave; C la valeur de B et &agrave; A la valeur de C (toujours quels que soient les contenus pr&eacute', 'VARIABLES A, B, C en ENTIER\r\nDEBUT\r\n    A &larr; 15\r\n    B &larr; 60\r\n    C &larr; 1000\r\n\r\n    A &larr; C\r\n    C &larr; B\r\n    B &larr; A\r\nFIN', 'A vaut 1000\r\nC vaut 60\r\nB vaut 15', 1),
(27, 'Exercice 1.8', 3, 'Que produit l&rsquo;algorithme suivant ?', 'Variables A, B, C en Caract&egrave;res\r\nD&eacute;but\r\n    A &larr; &quot;573&quot;\r\n    B &larr; &quot;19&quot;\r\n    C &larr; A + B\r\nFin', 'L&#039;op&eacute;rateur + effectue une concat&eacute;nation des deux cha&icirc;nes\r\nde caract&egrave;re.\r\n\r\nDonc:\r\n    A vaut &quot;573&quot;\r\n    B vaut &quot;19&quot;\r\n    C vaut &quot;57319&quot;', 1),
(28, 'Exercice 1.9', 3, 'Que produit l&rsquo;algorithme suivant ?', 'Variables A, B, C en Caract&egrave;res\r\nD&eacute;but\r\n    A &larr; &quot;573&quot;\r\n    B &larr; &quot;19&quot;\r\n    C &larr; A &amp; B\r\nFin', 'L&#039;op&eacute;rateur &amp; effectue une concat&eacute;nation des deux cha&icirc;nes\r\nde caract&egrave;re.\r\n\r\nDonc:\r\n    A vaut &quot;573&quot;\r\n    B vaut &quot;19&quot;\r\n    C vaut &quot;57319&quot;', 1),
(29, 'Exercice 2.1', 4, 'Quel r&eacute;sultat produit le programme suivant ?', 'VARIABLES val, double NUMERIQUES\r\nDEBUT\r\n    Val &larr; 231\r\n    Double &larr; Val * 2\r\n    Ecrire Val\r\n    Ecrire Double\r\nFIN', 'Val vaut 231\r\nDouble vaut 462', 1),
(30, 'Exercice 2.2', 4, 'Ecrire un programme qui demande un nombre &agrave; l&rsquo;utilisateur, puis qui calcule et affiche le carr&eacute; de ce nombre.', 'VARIABLES number, double NUMERIQUE\r\nDEBUT\r\n    ECRIRE &quot;Entrer un nombre:&quot;\r\n    LIRE number\r\n    double = number^2\r\n    ECRIRE double\r\nFIN\r\n', '', 1),
(31, 'Exercice 2.3', 4, 'Ecrire un programme qui lit le prix HT d&rsquo;un article, le nombre d&rsquo;articles et le taux de TVA, et qui fournit le prix total TTC correspondant. Faire en sorte que des libell&eacute;s apparaissent clairement.', '', '', 1),
(32, 'Exercice 2.4', 4, 'Ecrire un algorithme utilisant des variables de type cha&icirc;ne de caract&egrave;res, et affichant quatre variantes possibles de la c&eacute;l&egrave;bre &laquo; belle marquise, vos beaux yeux me font mourir d&rsquo;amour &raquo;. On ne se soucie pas de la ponctuation, ni des majuscules.', '', '', 1),
(33, 'Exercice 3.1', 8, 'Ecrire un algorithme qui demande un nombre &agrave; l&rsquo;utilisateur, et l&rsquo;informe ensuite si ce nombre est positif ou n&eacute;gatif (on laisse de c&ocirc;t&eacute; le cas o&ugrave; le nombre vaut z&eacute;ro).', '', NULL, 0),
(99, 'Exercice 3.2', 8, 'Ecrire un algorithme qui demande deux nombres &agrave; l&rsquo;utilisateur et l&rsquo;informe ensuite si leur produit est n&eacute;gatif ou positif (on laisse de c&ocirc;t&eacute; le cas o&ugrave; le produit est nul). Attention toutefois : on ne doit pas calculer le produit des deux nombres.', '', '', 0);

CREATE TABLE `algo_exercices_fields` (
  `slug` varchar(255) NOT NULL,
  `exercice_id` int(11) NOT NULL,
  `field_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `algo_exercices_fields` (`slug`, `exercice_id`, `field_name`) VALUES
('nbrArticle-31', 31, 'nbrArticle'),
('number-30', 30, 'number'),
('number-33', 33, 'number'),
('number_1-99', 99, 'number_1'),
('number_2-99', 99, 'number_2'),
('prixHT-31', 31, 'prixHT'),
('tauxTVA-31', 31, 'tauxTVA');


ALTER TABLE `algo_categories`
  ADD PRIMARY KEY (`cid`);

ALTER TABLE `algo_exercices`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `algo_exercices_fields`
  ADD UNIQUE KEY `slug` (`slug`);


ALTER TABLE `algo_categories`
  MODIFY `cid` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

ALTER TABLE `algo_exercices`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
