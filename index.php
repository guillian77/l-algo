<?php

// TODO: URL should be: controller/method/param1/param2/param3 ...
// TODO: Use class auto loader: see https://www.php.net/manual/fr/language.oop5.autoload.php

session_start();

/**
 * CONSTANTS
 */
define('ROOT', dirname($_SERVER['SCRIPT_NAME']));
// echo "<pre>";


/**
 * REQUIRES
 */
require_once 'config/config.php';
require_once 'models/Model.php';

/**
 * AUTO LOADER
 */
spl_autoload_register(function($className) {
    $isModel = strpos($className, 'Model');
    $isMiddleware = strpos($className, 'Middleware');

    if ($isModel) {
        require  "models/$className.php";
    }
    else if($isMiddleware) {
        require "controllers/middlewares/$className.php";
    }
    else {
        require  "controllers/$className.php";
    }
});

/**
 * ROUTER
 */
parse_str($_SERVER['QUERY_STRING'], $query);
$query = array_chunk($query, 2, TRUE);
(isset($query[0])) ? $request = (object) $query[0] : $request = null;
(isset($query[1])) ? $params = (object) $query[1] : $params = null;

// Start caching
// ob_start();

if (!empty($request->page))
{
    $controllerName = ucfirst($request->page) . "Controller";
    $controllerPath = "controllers/{$controllerName}.php";

    if (file_exists($controllerPath))
    {
        $action = $request->action;
        $controller = new $controllerName($params);
        $controller->$action($params);
    }
    else
    {
        require 'views/error/404.php';
    }
}
else
{
    new HomeController();
}
