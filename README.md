# Installation
- Rename **config.sample.php** into **config.php** located in **/config/**.
- Edit this **config.php** with your settings.
- Pull database file.
# Libraries
#### Font Awesome
Implement nice icons.
https://fontawesome.com
#### Prism
Used for code blocks stylizing.
https://prismjs.com
# Fonts
#### Leckerli One
Used on main logo.
https://fonts.google.com/specimen/Leckerli+One?query=Leckerli
#### Roboto
For rest of the app.
https://fonts.google.com/specimen/Roboto

# TODO
- Add a little authentification system to prevent bad guys.
- Create Database export code.
- Refact app.js.
- Import external libraries without CDN.
# Done
- Add fields creation on exercice edition.
- Automatic script file creation on exercice creation. [Done]
