/**
 * -------------------------------------------------------------------------
 * - Left menu
 * -------------------------------------------------------------------------
 */

/**
 * Toggle left menu sub categories: display/hide.
 */
var toggleCollaps = function () {
    /* Select all node elements called collaps */
    var trigger = document.querySelectorAll(".collaps");
    var i;

    for (i = 0; i < trigger.length; i++)
    {
        trigger[i].addEventListener("click", function(e) {

            /* Delete default redirection on links */
            e.preventDefault()

            /* Look for next element */
            var content = this.nextElementSibling;

            /* Check if next element is needed */
            if (content.classList.contains('collaps__content'))
            {
                /* Get computed display type */
                var type = getComputedStyle(content)
                type = type.display

                /* Switch style */
                if (type === "block") {
                    content.style.display = "none";
                }
                else {
                    content.style.display = "block";
                }
            }
        
        });
    }
}()

/**
 * Put exercice status icon
 */
var exerciceStatus = function() {

    /* Look for exercices are dones */
    var doneElement = document.querySelectorAll('.done')

    for(var i = 0; i < doneElement.length; i++) {

        /* Create font awesome element */
        var checkedIcon = document.createElement("i");
        checkedIcon.classList.add('fas', 'fa-check')
        checkedIcon.style.color = "green"

        /* Append FA icon to element */
        doneElement[i].appendChild(checkedIcon)
    }

    /* Look for exercices are inwork */
    var inworkElement = document.querySelectorAll('.inwork')

    for(var i = 0; i < inworkElement.length; i++) {

        /* Create font awesome element */
        var checkedIcon = document.createElement("i");
        checkedIcon.classList.add('fas', 'fa-hourglass-start')
        checkedIcon.style.color = "orange"

        /* Append FA icon to element */
        inworkElement[i].appendChild(checkedIcon)
    }
}()

/**
 * -------------------------------------------------------------------------
 * - Forms : Categories
 * -------------------------------------------------------------------------
 */

/**
 * Get parent categorie ID from select
 * @param {*} callback Parent categorie ID
 */
var getSelectedMainCategorie = function (callback) {
    var select = document.querySelector('#main_categorie')
    select.addEventListener("change", function(e) {
        var selected = select.options[select.selectedIndex].value
        return callback(selected)
    })
}

/**
 * Display hidden select tag on parent select change
 */
var displayHiddenSelect = function () {
    var hiddenSelect = document.querySelector('.hidden-select')

    if (!hiddenSelect) { return; }

    getSelectedMainCategorie(async function(selectedCategorie) {
        var options = hiddenSelect.querySelectorAll('select > option')

        options.forEach(function(option) {

            // Reset style before
            option.style.display = "block"

            // Hide sub categorie are'nt in parent
            if (option.dataset.parent != selectedCategorie) {
                option.style.display = "none"
            }
        })
        hiddenSelect.style.display = "block"
    })
    
}()

let customsFields = function() {
    let addButton = document.querySelector('#addCustomField');

    if (!addButton) { return; }

    // Init count
    let count = 0;

    let lastField;

    let onLoadFields = getCurrentFields();
    let onLoadCrosses;

    // If there already are some fields
    if (onLoadFields)
    {
        // Update count
        count = onLoadFields.length;
        lastField = updateLastField();

        // Listen onload fields for suppression
        onLoadCrosses = getCurrentCrosses();
        onLoadCrosses.forEach(function(cross) {
            listenDeletion(cross)
        })
    }

    /**
     * Get current fields nodes
     * @return {NodeList} Current fields node list
     */
    function getCurrentFields() {
        return document.querySelectorAll('.custom-field.placeholder');
    }

    function getCurrentCrosses() {
        return document.querySelectorAll('.delete-cross');
    }

    /**
     * Update last field
     * @return {Node} lastField The last field node found.
     */
    function updateLastField() {
        var nodes = getCurrentFields()
        lastField = nodes[nodes.length -1];
        return lastField;
    }

    /**
     * Create an element, after an other one.
     * @param {*} referenceNode The Node element reference, to put afer.
     * @param {*} newNode The Node to create after the reference.
     */
    function insertAfter(referenceNode, newNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
        
        let node = {
            reference: referenceNode,
            new: newNode
        }

        return node;
    }

    /**
     * Create a new input for futur custom field name
     * @param Number number
     */
    function createInput(number) {
        input =  document.createElement('input')
        input.type = "text"
        input.classList.add('custom-field', 'input', 'input-lg', 'mt-1')
        input.placeholder = "Input name"
        input.name = "customField[" + number + "][name]"
        input.dataset.fieldnum = number
        return input
    }

    /**
     * Create a new deletion cross after a new input
     * @param {*} number 
     */
    function createDeleteCross(number) {
        cross = document.createElement('span')
        cross.innerHTML = '<i class="far fa-times-circle"></i>'
        cross.classList.add('delete-cross')
        cross.dataset.fieldnum = number
        return cross
    }

    /**
     * Create a new input for futur custom field placeholder
     * @param {*} number 
     */
    function createPlaceholder(number) {
        placeholder = document.createElement('input');
        placeholder.type = "text";
        placeholder.classList.add('custom-field', 'input', 'input-lg', 'placeholder');
        placeholder.placeholder = "Input placeholder";
        placeholder.name = "customField[" + number + "][placeholder]"
        placeholder.dataset.fieldnum = number;
        return placeholder;
    }

    /**
     * Listen click on deletion crosses
     * @param {*} cross 
     */
    function listenDeletion(cross) {
        cross.addEventListener('click', function(event) {
            var fieldnum = cross.dataset.fieldnum
            var elementToRemove = document.querySelectorAll('[data-fieldnum="' + fieldnum + '"]')
            elementToRemove.forEach(function(element) {
                element.remove();
            })

            updateLastField()
        })
    }
    

    // Listen click on created customs fields inputs
    addButton.addEventListener('click', function (event) {
        event.preventDefault()

        var stmt;
        
        // If there is no customs fields yet,
        // create the first after add button.
        // Else: create after last element.
        if (lastField === undefined || lastField === null)
        {
            stmt = insertAfter(addButton, createInput(count))
            lastField = stmt.new
            stmt = insertAfter(lastField, createDeleteCross(count))
            lastField = stmt.new
            listenDeletion(stmt.new)
            stmt = insertAfter(lastField, createPlaceholder(count))
            lastField = stmt.new
        }
        else
        {
            stmt = insertAfter(lastField, createInput(count))
            lastField = stmt.new
            stmt = insertAfter(lastField, createDeleteCross(count))
            lastField = stmt.new
            listenDeletion(stmt.new)
            stmt = insertAfter(lastField, createPlaceholder(count))
            lastField = stmt.new
        }

        count++;
    })

}()

/**
 * -------------------------------------------------------------------------
 * - Exercices : Show
 * -------------------------------------------------------------------------
 */

/**
 * Display the great code block with buttons
 */
let languagesSelector = function () {

    let selectors = document.querySelectorAll('.code-selector button')

    selectors.forEach( (selector) => {
        // Wait for click
        selector.addEventListener('click', (event) => {
            let target = event.target.dataset.type
            let pres = document.querySelectorAll('.code-displayer .codeblock pre')
            
            pres.forEach( (pre) => {
                if (pre.id === target) {
                    pre.classList.add('show')
                } else {
                    pre.classList.remove('show')
                }
            })
        })
    })
}()

const clientButtons = ['javascript', 'jquery']
var currentTarget = 'javascript' // Default
let sendButton = document.querySelector('.input-displayer .btn')
let selectors = document.querySelectorAll('.code-selector button')

/**
 * Define the current target
 * @param {*} target 
 */
let defineTarget = function (target) {
    currentTarget = target
}

/**
 * Define currentTarget on selector click event
 */
selectors.forEach(function(selector) {
    selector.addEventListener('click', function(event) {
        defineTarget(event.target.dataset.type)
    })
})

/**
 * Disallow page reload on client side scripts
 */
if (sendButton) {
    sendButton.addEventListener('click', function (event) {
        // Disallow send button on client side scripts
        if (clientButtons.includes(currentTarget))
        {
            event.preventDefault()

            // Call script function named by type [javascript | jquery]
            window[currentTarget]()
        }
    })
}

